<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->


		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<font>
					<table rules="rows" border="1" width="100%">
						<tr>
							<td align="center"><a href="o_detail.html"> <img
									src="photo/outer/outer1.jpg" /></a></td>
							<td align="center"><img src="photo/outer/outer2.jpg">
							</td>
							<td align="center"><img src="photo/outer/outer3.jpg">
							</td>
						</tr>

						<tr bgcolor="#FFEBCD">
							<td align="center">星柄フリースジャケット<br>￥1,377(税込)
							</td>
							<td align="center">カラーボタン付きカーディガン<br>￥1,569(税込)
							</td>
							<td align="center">花柄フリースジャケット<br>￥2,058(税込)
							</td>
						</tr>

						<tr>
							<td align="center"><img src="photo/outer/outer5.jpg" /></td>
							<td align="center"><img src="photo/outer/outer6.jpg" /></td>
							<td align="center"><img src="photo/outer/outer7.jpg" /></td>
						</tr>

						<tr bgcolor="#FFEBCD">
							<td align="center">GOGO☆ブルゾン風ジャケット<br>￥1,569(税込)
							</td>
							<td align="center">スクールロゴ入りダッフルコート風アウター<br>￥1,552(税込)
							</td>
							<td align="center">ハートロゴ入りパーカー<br>\1,069(税込)
							</td>
						</tr>


						<tr>
							<td align="center"><img src="photo/outer/outer8.jpg"
								width="400" height="400"></td>
							<td align="center"><img src="photo/outer/outer9.jpg"
								width="400" height="400"></td>
							<td align="center"><img src="photo/outer/outer10.jpg"
								width="400" height="400"></td>
						</tr>

						<tr bgcolor="#FFEBCD">
							<td align="center">マリン柄ジャケット<br>￥1,069(税込)
							</td>
							<td align="center">車ロゴ入りニットベスト<br>￥2,052(税込)
							</td>
							<td align="center">スノー柄フリースジャケット<br>\1,369(税込)
							</td>
						</tr>


						<tr>
							<td align="center"><img src="photo/outer/outer4.jpg"
								width="400" height="400"></td>
						</tr>
						<tr bgcolor="#FFEBCD">
							<td align="center">リボン付きダウンジャケット<br>￥2,569(税込)
							</td>
						</tr>
					</table>
					<div align="center">
						<center>
							<a href="outer.html"><img src="buttom/pgup_b.gif"
								alt="TAG index" border="0"></a>
						</center>
					</div>





				</font>
			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html>
