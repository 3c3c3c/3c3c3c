<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<p style="margin: 20px;">
						<font>======================お客様情報入力=======================</font>
					</p>

					<p style="margin: 20px;">
						<font><b> アカウントを新規作成します</b></font>
					</p>
					<b style="color:red;font-size:20px;"><html:errors/></b>

					<html:form action="makeUserAction" method="post">
						<table border="1">
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>姓
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family" style="width: 210px; height: 30px" />
								</td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>名
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first" style="width: 210px; height: 30px" />
								</td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>せい
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family_ruby" style="width: 210px; height: 30px" />
								</td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>めい
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first_ruby" style="width: 210px; height: 30px" />
								</td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>郵便番号</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="postal"
									style="width: 100px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>都道府県</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_prefectural"
									style="width: 570px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>市町村</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_city"
									style="width: 570px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>番地</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_town"
									style="width: 570px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">アパート・マンション名</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_location"
									style="width: 570px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>電話番号</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="tel"
									style="width: 150px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>メールアドレス</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="mail"
									style="width: 570px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>パスワード</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:password property="pass"
									style="width: 150px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>パスワード再入力</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:password property="repass"
									style="width: 150px; height: 30px" /></td>
							</tr>
						</table>

						<p style="margin: 15px;">
							<b style="font-size: 24px;"><span style="color: red">※</span>は必須項目です</b>
						</p>

						<p style="margin: 15px;">
							<html:image src="buttom/decide_conf.gif" alt="TAG index" border="0" /> </a>
						</p>
					</html:form>
				</div>
			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>