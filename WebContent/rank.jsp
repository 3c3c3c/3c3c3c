<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="jp.co.ccc.user.form.ProductForm"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="java.util.ArrayList"%>
<%ArrayList<ProductForm>rankInfo = (ArrayList<ProductForm>) request.getAttribute("rankInfo");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
<LINK rel="stylesheet" type="text/css" href="timesale.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">

				<div align="center">
					<br> <font><b style="color: #000066;">☆売れ筋ランキング☆</b></font>


					<div id="bdr3" align="center">
						<div id="contents">
							<table class="style3">
								<tr>
									<td class="bottom" align="center"
										style="width: 300px; padding-left: 10px;">
										<div>
											<img src="img/1.png">
										</div>
										<div>
											<html:form method="post" action="/detailAction">
												<input type="image"src="photo/<%=rankInfo.get(0).getProductimage()%>"alt=""
													width="180px" height="200px"/><br>
												<%=rankInfo.get(0).getProductname()%><br>
												<html:hidden property="productid" value='<%=Integer.toString(rankInfo.get(0).getProductid())%>' />
											</html:form>
										</div>
									</td>
									<td class="bottom" align="center"
										style="width: 300px; padding-left: 10px;">
										<div>
											<img src="img/2.png">
										</div>
										<div>
											<html:form method="post" action="/detailAction">
												<input type="image"
													src="photo/<%=rankInfo.get(1).getProductimage()%>"
													width="180px" height="200px"alt="" /><br>
												<%=rankInfo.get(1).getProductname()%><br>
												<html:hidden property="productid" value='<%=Integer.toString(rankInfo.get(1).getProductid())%>' />
											</html:form>
										</div>
									</td>
									<td class="bottom" align="center"
										style="width: 300px; padding-left: 10px;">
										<div>
											<img src="img/3.png">
										</div>
										<div>
											<html:form method="post" action="/detailAction">
												<input type="image"
													src="photo/<%=rankInfo.get(2).getProductimage()%>"
													width="180px" height="200px" name="button" alt="" /><br>
												<%=rankInfo.get(2).getProductname()%><br>
												<html:hidden property="productid" value='<%=Integer.toString(rankInfo.get(2).getProductid())%>' />
											</html:form>
										</div>
									</td>
								</tr>

								<tr>
									<td class="bottom" align="center"
										style="width: 250px; padding-left: 10px;">
										<div>
											<img src="img/runk4.png">
										</div>
										<div>
											<html:form method="post" action="/detailAction">
												<input type="image"
													src="photo/<%=rankInfo.get(3).getProductimage()%>"
													width="180px" height="200px" name="button" alt="" /><br>
												<%=rankInfo.get(3).getProductname()%><br>
												<html:hidden property="productid" value='<%=Integer.toString(rankInfo.get(3).getProductid())%>' />
											</html:form>
										</div>
									</td>
									<td class="bottom" align="center"
										style="width: 250px; padding-left: 10px;">
										<div>
											<img src="img/runk5.png">
										</div>
										<div>
											<html:form method="post" action="/detailAction">
												<input type="image"
													src="photo/<%=rankInfo.get(4).getProductimage()%>"
													width="180px" height="200px" name="button" alt="" /><br>
												<%=rankInfo.get(4).getProductname()%><br>
												<html:hidden property="productid" value='<%=Integer.toString(rankInfo.get(4).getProductid())%>' />
											</html:form>
										</div>
									</td>

									<td class="bottom" align="center"
										style="width: 250px; padding-left: 10px;">
										<div>
											<img src="img/runk6.png">
										</div>
										<div>
											<html:form method="post" action="/detailAction">
												<input type="image"
													src="photo/<%=rankInfo.get(5).getProductimage()%>"
													width="180px" height="200px" name="button" alt="" /><br>
												<%=rankInfo.get(5).getProductname()%><br>
												<html:hidden property="productid" value='<%=Integer.toString(rankInfo.get(5).getProductid())%>' />
											</html:form>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>