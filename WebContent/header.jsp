<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="jp.co.ccc.user.form.UserInfoForm" %>


<% String name = "";
if( session.getAttribute("login") != null ){
	UserInfoForm user = (UserInfoForm)session.getAttribute("login");
	name = user.getUsername_family() + user.getUsername_first();
}
%>

<div id="header">
	<table>
		<tr>
			<td>
				<html:link page="/index.jsp"> <html:img page="/logo6new.png" /> </html:link>
			</td>
			<td class="usrs" style="font-size: 20px; width: 350px;">
				<% if( session.getAttribute("login") != null ){ %>
					<html:link page="/userpage.jsp" ><font color=white><%= name %>さんこんにちは</font></html:link>
				<%	}else{ %>
					<font color=white>ゲストさんこんにちは</font>
				<%	} %>
			</td>
			<td class="usrs" style="width: 160px;">
				<% if( session.getAttribute("login") != null ){ %>
					<html:link page="/withdraw.jsp"> <html:img page="/buttom/withdraw_b.gif" alt="TAG index" border="0" /> </html:link>
				<%	}else{ %>
					<html:link page="/makeuser.jsp"> <html:img page="/buttom/makeuser.gif" alt="TAG index" border="0" /> </html:link>
				<%	} %>
			</td>

			<td class="usrs" style="width: 130px;">
				<html:form action="/logoutAction" >
					<% if( session.getAttribute("login") != null ){ %>
							<html:image page="/buttom/logout.gif" />
					<%	}else{ %>
							<html:link page="/login.jsp"><html:img page="/buttom/login_b.gif" /></html:link>
					<%	} %>
				</html:form>
			</td>
		</tr>
	</table>
</div>