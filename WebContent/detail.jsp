<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jp.co.ccc.user.form.ProductForm"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%
	ArrayList<ProductForm> detail = (ArrayList<ProductForm>) request.getAttribute("detail");
	int[][] sizerange = {{80, 90, 100, 110}, {48, 50, 52, 54}, {12, 14, 16, 18}};
	int[][] numrange = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Detail</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<html:form action="/listupAction">
						<html:hidden property="productid"
							value="<%=Integer.toString(detail.get(0).getProductid())%>" />
						<table border="1" style="border: solid 3px #FF4F02; margin: 15px;">
							<tr>
								<td align="center" rowspan="6"><img
									src="photo/<%=detail.get(0).getProductimage()%>"
									width="400px" height="450px" /></td>
								<td align="center"
									style="width: 130px; height: 70px; background-color: #FFFFBB">

									<html:select property="num" style="height: 50px;" value="1">
										<html:option value="1">1</html:option>
										<html:option value="2">2</html:option>
										<html:option value="3">3</html:option>
										<html:option value="4">4</html:option>
										<html:option value="5">5</html:option>
									</html:select><font>個</font>

								</td>
							</tr>

							<tr>
								<td align="center"
									style="width: 200px; height: 100px; background-color: #FFFFBB">

								<% int[] size, num;
									if( detail.get(0).getCategory() >= 1 && detail.get(0).getCategory() <= 9 ){
										size = sizerange[0];
										num = numrange[0];
									}else if( detail.get(0).getCategory() >= 10 && detail.get(0).getCategory() <= 12 ){
										size = sizerange[1];
										num = numrange[1];
									}else{
										size = sizerange[2];
										num = numrange[2];
									}
								%>
									<html:select property="size" style="height: 50px;" value='<%= Integer.toString(num[0]) %>'>
										<html:option value='<%= Integer.toString(num[0]) %>'><%= size[0] %></html:option>
										<html:option value='<%= Integer.toString(num[1]) %>'><%= size[1] %></html:option>
										<html:option value='<%= Integer.toString(num[2]) %>'><%= size[2] %></html:option>
										<html:option value='<%= Integer.toString(num[3]) %>'><%= size[3] %></html:option>
									</html:select><font>サイズ</font>
								</td>
							</tr>

							<tr>
								<td align="center"
									style="height: 100px; background-color: #FFFFBB"><html:image
										src="buttom/incart.gif" alt="TAG index" border="0">
									</html:image></td>
							</tr>
							<tr>
								<td align="center" style="background-color: #FFFFBB"><font><b>＜値段(税込)＞<br>\<%=detail.get(0).getPrice()%></b></font>
								</td>
							</tr>
							<tr>
								<td align="center" style="background-color: #FFFFBB"><font>在庫：<%=detail.get(0).getStock()%>点
								</font></td>
							</tr>
							<tr>
								<td align="center" colspan="2" style="background-color: #FFFFBB">
									<img src="buttom/fit.gif" alt="TAG index" border="0" style="cursor: pointer;"
									onclick="window.open('goFit.jsp?'+<%= detail.get(0).getProductid() %>, 'new', 'width=600, height=775, scrollbars=0');">
								</td>
							</tr>
							<tr>
								<td align="left" colspan="2" style="background-color: #FFFFBB">
									<p align="center" style="margin: 10px;">
										<font style="font-size: 36px;"><%=detail.get(0).getProductname()%></font>
									</p> <font style="font-size: 20px;"> <b
										style="font-size: 24px;">＜仕様＞</b><br> <%=detail.get(0).getMeasure()%><br>
										<b style="font-size: 24px;">＜取扱い＞</b><br> <%=detail.get(0).getTransaction()%><br>
										<b style="font-size: 24px;">＜コメント＞</b><br> <%=detail.get(0).getComment()%><br>
										<br>
								</font>
								</td>
							</tr>
						</table>
					</html:form>
				</div>

			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>