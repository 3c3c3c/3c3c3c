<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@ page import = "java.util.ArrayList,jp.co.ccc.entity.CartEntity" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>カート一覧画面</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div id="contents" align="center">
					<p style="margin: 15px;">
						<font><b>買い物かごの中身一覧</b></font>
					</p>

						<table border="1">
							<tr align="center" style="background-color: #CCFF99; font-weight: bold;">
								<td height="30px">画像</td>
								<td width="200px">商品名</td>
								<td>サイズ</td>
								<td>単価</td>
								<td width="140px">数量</td>
								<td width="130px">合計金額</td>
								<td>削除</td>
							</tr>

							<% ArrayList<CartEntity> cart = (ArrayList<CartEntity>)session.getAttribute("cartent"); %>
							<% int total = 0; %>
							<% if( cart != null ){ %>
							<logic:iterate id="data" collection="<%= cart %>" indexId="idx" >
								<% CartEntity cartent = (CartEntity)data; %>
								<tr align="center" style="background-color: white">
									<td>
										<html:img page='<%= "/photo/"+ cartent.getImage() %>' height="100" width="100" /> <br>
									</td>
									<td>
										<%= cartent.getName() %>
									</td>
									<td>
										<%= cartent.getSize() %>
									</td>
									<td>
										\ <%= cartent.getPrice() %> 円(税込)
									</td>
									<td>
										<html:form action="cartUpdateAction" >
											<html:select property="num" value='<%= Integer.toString(cartent.getQuantity()) %>' >
												<html:option value="1">1</html:option>
												<html:option value="2">2</html:option>
												<html:option value="3">3</html:option>
												<html:option value="4">4</html:option>
												<html:option value="5">5</html:option>
											</html:select> 個
											<html:hidden property="productid" value='<%= Integer.toString(idx) %>' />
											<html:image page="/buttom/koushin.gif" value="update"  />
										</html:form>
									</td>
									<td>
										\ <%= cartent.getPrice() * cartent.getQuantity() %> 円(税込)
										<% total += cartent.getPrice() * cartent.getQuantity(); %>
									</td>
									<td>
										<html:form action="cartDeleteAction" >
											<html:hidden property="productid" value='<%= Integer.toString(idx) %>' />
											<html:image page="/buttom/sakujo.gif" />
										</html:form>
									</td>
								</tr>
							</logic:iterate>
							<% } %>

							<tr align="right" style="background-color: white">
								<td colspan="7" style="height: 40px;">送料：500円</td>
							</tr>
							<tr align="right" style="background-color: white">
								<td colspan="7" style="height: 40px;"><b
									style="font-size: 24px;">合計：<%= total + 500 %>円</b></td>
							</tr>
						</table>
						<br>

					<html:form action="buyAction">
						<html:image page="/buttom/chumontetudukihe.gif" alt="TAG index" border="0" />
					</html:form>
				</div>
			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>