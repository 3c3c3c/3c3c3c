<%@ page language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<%
				String mail = (String)request.getAttribute("mail");
			%>
			<html:form action="/rePasswordFormAction" method="post">
			<div id="boxB"align="center">
				<br>
				<center>
					<img src="img/passprocess4.png"width="900px"height="100px" />
				</center>
				<br> <br>
				<table border="1" cellspacing="0" bgcolor="white" width="700"
					height="120" align="center">
					<tr>
						<td align="center" style="background-color: #CCFF99;">
							新しいパスワード</td>
						<td style="padding: 20px;"><br><html:text property = "pass" size="70" />
						 <br> <br> <span style="color: red">※</span>
						 確認用に再入力してください<br>
							<html:text property="repass" size="70" /> <br> <br></td>
					</tr>
				</table>
				<br> <br>
				<center>
					<html:image
						src="buttom/decide.gif" alt="TAG index" border="0" />
				</center>
			</div>
			<html:hidden property="mail" value='<%=mail%>' />
			</html:form>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>