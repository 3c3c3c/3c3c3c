<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TOP画面</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
<LINK rel="stylesheet" type="text/css" href="slideshow.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<script src="http://www.google.com/jsapi"></script>
				<script>
					google.load("jquery", "1.7");
					google.load("jqueryui", "1.8");
				</script>

				<div class="slide">
					<div class="item">
						<p>
							<html:form action="/rankingAction">
								<html:image page="/img/top.png" />
							</html:form>
						</p>
					</div>
					<div class="item">
						<p>
							<html:form action="/clearanceAction">
								<html:image page="/img/top2.png" />
							</html:form>
						</p>
					</div>
					<div class="item">
						<p>
							<a style="cursor: pointer" onclick="window.open('goFit.jsp', 'new', 'width=600, height=775, scrollbars=0');">
								<html:img page="/img/top3.png" />
							</a>
						</p>
					</div>
					<!-- / .slide -->
				</div>

				<script type="text/javascript" src="slideshow.js"></script>

				<div id="thumbnail" align="center">
					<table>
						<tr>
							<td>
								<html:form action="/clearanceAction">
									<html:image page="/img/top.png" />
								</html:form>
							</td>
							<td>
								<a style="cursor: pointer" onclick="window.open('goFit.jsp', 'new', 'width=600, height=775, scrollbars=0');">
									<html:img page="/img/top2.png" />
								</a>
							</td>
							<td>
								<html:form action="/rankingAction">
									<html:image page="/img/top3.png" />
								</html:form>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>