<%@ page language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
<LINK rel="stylesheet" type="text/css" href="timesale.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<%
				String mail = (String)request.getAttribute("mail");
			%>
			<html:form action="/rePasswordSendMailAction" method="post">
			<div id="boxB" align="center">

				<br>
				<center>
					<img src="img/passprocess2.png" width="900px"height="100px"/>
				</center>

				<br> <br>
				<center>
					<font size="4">確認メール送信完了</font><br>
				</center>
				<table border="2"  bgcolor="white" width="700" rules="rows"
					height="200" align="center">
					<tr>
						<td align="center">
						<img src="photo/email.jpg" width="100" height="100" style="align:center;" hspace="20px">
						</td>
						<td align="center">確認メールの送信が完了しました。<br>
							メールが到着している事をご確認ください。<br>
						<br></td>
					</tr>
				</table>

				<br> <br>
				<b style="color:red;font-size:20px;"><html:errors /></b>
				<table border="1" cellspacing="0" bgcolor="white" width="700"
					height="100" align="center">
					<tr>
						<td><br>
							<center>メールで指定されたパスワードを入力してください</center>
							<center>

								<html:text property="mail" />
							</center> <br>
							<center>
								<html:image
									src="buttom/send.gif" alt="TAG index" border="0" />
							</center></td>
						</tr>
				</table>
			</div>
			</html:form>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>
