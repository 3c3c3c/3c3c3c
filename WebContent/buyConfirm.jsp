<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>購入確認画面</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<p style="margin: 30px;">
						<font>ログインしてください</font>
					</p>

					<form action="itemConfirm.html" method="post" style="margin: 30px;">
						<table border="1" style="width: 700px; ">
							<tr>
								<td style="padding: 20px; background-color: #CCFF99;">
									<b> <span style="color: red">※</span>メールアドレス: </b>
								</td>
								<td style="padding: 20px; background-color: white;">
									<input type="text" name="id" size="70">
								</td>
							</tr>
							<tr>
								<td style="padding: 20px; background-color: #CCFF99;">
									<b> <span style="color: red">※</span>パスワード: </b>
								</td>
								<td style="padding: 20px; background-color: white;">
									<input type="password" name="name" size="70">
								</td>
							</tr>
						</table>
					</form>
					<b style="color: red; font-size: 25px;">※全て必須項目です。</b>

					<p>
						<a href="delivery.html"> <img src="buttom/login_b.gif" alt="TAG index" border="0"> </a>
						<a href="makeuser.html"> <img src="buttom/makeuser.gif" alt="TAG index" border="0"> </a>
					</p>

					<font>パスワードを忘れた方は<a href="rePassword.html">こちら</a></font>
				</div>

			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html>