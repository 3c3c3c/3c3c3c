<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<html:html>
<head>
<title>Fitting</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="fit.css">

<script type="text/javascript" src="fit.js"></script>
</head>
<body>
	<div id="fitting">
		<% String fitimg = (String)request.getAttribute("fitimg"); %>
		<% String image = (String)request.getAttribute("image"); %>

		<img src="<html:rewrite page='<%= "/PermeationImage/"+ fitimg %>' />" class="move" name="img1" onMousedown="dragOn('img1')" onmousewheel="zoom('img1')" style="top: 50px; left: 50px; z-index: 2;">
		<html:img page='<%= "/upload/"+ image %>' styleClass="set" />

		<div>
			<b><font size="3">商品画像をマウスでドラッグする事で移動できます。<br>
			 オンマウス状態でスクロールホイールすることで拡大・縮小できます。</font></b>
		</div>
	</div>

	<div align="center">
		<html:img page="/buttom/close_b.gif" onclick="window.close()" style="cursor: pointer" alt="TAG index" border="0" />
	</div>

</body>
</html:html>