<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Credit</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<p align="center" style="margin: 20px;">
						<html:img page="/img/process02.png" width="900" height="120" />
					</p>

					<html:form action="/payAction">
					<b style="color:red;font-size:20px;"><html:errors/></b>
						<table border="1">
							<tr>
								<td align="center" style="width: 50px; height: 30px; font-weight: bold; background-color: #CCFF99;" colspan="3">
									お支払方法の選択</td>
							</tr>
							<tr>
								<td align="left" style="width: 200px; height: 30px; font-weight: bold; padding-left: 10px; background-color: #CCFF99;" colspan="3">
									<html:radio property="payway" value="credit" />
										<span style="color: red">※</span>クレジット</td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; height: 30px; font-weight: bold; background-color: #CCFF99;">カード会社</td>
								<td align="left" style="width: 420px; font-weight: bold; padding-left: 10px; background-color: white;">
									<html:text property="cardoffice" style="width: 210px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; height: 30px; font-weight: bold; background-color: #CCFF99;">カード番号</td>
								<td align="left" style="width: 420px; font-weight: bold; padding-left: 10px; background-color: white;">
									<html:text property="cardnum" style="width: 210px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; height: 30px; font-weight: bold; background-color: #CCFF99;">有効期限</td>
								<td align="left" style="width: 420px; font-weight: bold; padding-left: 10px; background-color: white;">
									<html:text property="cardlimit" style="width: 210px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; height: 30px; font-weight: bold; background-color: #CCFF99;">カード名義人</td>
								<td align="left" style="width: 420px; font-weight: bold; padding-left: 10px; background-color: white;">
									<html:text property="cardname" style="width: 210px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; height: 30px; font-weight: bold; background-color: #CCFF99;">セキュリティコード</td>
								<td align="left" style="width: 420px; font-weight: bold; padding-left: 10px; background-color: white;">
									<html:text property="cardsecurity" style="width: 210px; height: 30px" /></td>
							</tr>
							<tr>
								<td align="left" style="width: 200px; height: 30px; font-weight: bold; padding-left: 10px; background-color: #CCFF99;" colspan="3">
									<html:radio property="payway" value="cod" />
										<span style="color: red">※</span>代金引換</td>
							</tr>
						</table>

						<p align="center" style="margin: 20px;">
							<html:image page="/buttom/chumonhe.gif" alt="TAG index" border="0" />
						</p>
					</html:form>
				</div>
			</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html>