<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>カート一覧画面</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div id="contents" align="center">
					<p style="margin: 15px;">
						<font><b>カートの中身一覧</b></font>
					</p>
					<br>
					<table border="1">
						<tr align="center"
							style="background-color: #CCFF99; font-weight: bold;">
							<td height="30px">画像</td>
							<td width="200px">商品名</td>
							<td>サイズ</td>
							<td>単価</td>
							<td width="140px">数量</td>
							<td width="100px">合計金額</td>
							<td>削除</td>

						</tr>
						<tr align="center" style="background-color: white">
							<td align="center"><img src="photo/shirt/shirt1.jpg"
								height="100" width="100" /></td>
							<td>ハートロゴTシャツ</td>
							<td>80</td>
							<td>￥1,177(税込)</td>
							<td><select name="数量" style="font-size: 20px;">
									<option value="1">1</option>
									<option value="2" selected>2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
							</select>個<a href="cartafter.html"><img src="buttom/koushin.gif"
									alt="TAG index" border="0"></a>
							<td>2,354円</td>
							<td><a href="cart.html"><img src="buttom/sakujo.gif"
									alt="TAG index" border="0"></a></td>

						</tr>

						<tr align="right" style="background-color: white">
							<td colspan="7" style="height: 40px;">送料:500円</td>
						</tr>
						<tr align="right" style="background-color: white">
							<td colspan="7" style="height: 40px;"><b>合計:2,854円</b></td>
						</tr>
					</table>
					<br>
						<a href="buyConfirm.html"><img src="buttom/chumontetudukihe.gif" alt="TAG index" border="0"></a>
				</div>
				<!--
	<form action="delete.html">
		<input type="submit" value="削除"><br> <br> <a
			href="buyConfirm.html"><font>購入</font></a><br>
	</form> -->
			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html>