<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<%@ page import = "java.util.ArrayList, jp.co.ccc.entity.CartEntity" %>
<%
	ArrayList<CartEntity> cart = (ArrayList<CartEntity>)session.getAttribute("cartent");
	int quantity = 0;
	int total = 0;
	if( cart != null ){
		for( CartEntity data : cart ){
			quantity += data.getQuantity();
			total += data.getPrice() * data.getQuantity();
		}
	}
%>


<ul>
	<li2>
		<a>カート商品</a>
	</li2>
	<li>
		<a>
		<html:form action="/cartAction" >
			<table width=260px cellspacing="0">
				<tr>
					<td bgcolor="#FFE4E1" align="center">
						<html:image src="cart3.png" />
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFE4E1">商品個数 : <%= quantity %>個</td>
				</tr>
				<tr>
					<td bgcolor="#FFE4E1">合計金額 : \<%= total %></td>
				</tr>
			</table>
		</html:form>
		</a>
	</li>
	<html:form action="/clearanceAction">
	<li><html:link action="/clearanceAction">在庫処分セール</html:link>
	</li>
	</html:form>
	<li style="cursor: pointer">
		<a onclick="window.open('goFit.jsp', 'new', 'width=600, height=775, scrollbars=0');">フィッティング</a></li>
	<li>
		<html:form action="/rankingAction">
		<html:link action="/rankingAction">ランキング</html:link>
		</html:form>
	</li>
</ul>