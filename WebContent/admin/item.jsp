<%@page import="jp.co.ccc.admin.form.ProductForm"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% ProductForm productdata = (ProductForm)request.getAttribute("productdata"); %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Item</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">
			<div id="bdr2">
				<table>
					<tr class="dsh">
						<td rowspan="4">
							<html:img page='<%= "/photo/"+ productdata.getImage() %>'
								height="200px" width="200px" style="margin-right: 10px;" /></td>
						<td>ID</td>
						<td>
							<%=Integer.toString(productdata.getProductid()) %>
						</td>
						<% int id = productdata.getProductid(); %>
					</tr>
					<tr class="dsh">
						<td>商品名</td>
						<td><%= productdata.getName() %></td>
					</tr>
					<tr class="dsh">
						<td>販売価格</td>
						<td><%= productdata.getPrice() %>円</td>
					</tr>
					<tr class="dsh">
						<td>在庫</td>
						<td><%= productdata.getStock() %>点</td>
					</tr>
				</table>
				<table>
					<tr class="dsh">
						<td>仕様</td>
						<td><%= productdata.getWay() %></td>
					</tr>
					<tr class="dsh">
						<td>取扱い</td>
						<td><%= productdata.getManagement() %></td>
					</tr>
					<tr class="dsh">
						<td>コメント</td>
						<td><%= productdata.getComment() %></td>
					</tr>
				</table>

				<div align="center">
					<table class="ud">
						<tr>
							<td width="20" align="center">
								<html:form action="/ProductUpdateAction" method="post">
									<html:image page="/buttom/update_b.gif" />
									<html:hidden property="productid" value='<%= Integer.toString(productdata.getProductid()) %>' />
									<html:hidden property="name" value="xxx" /><html:hidden property="way" value="xxx" /><html:hidden property="management" value="xxx" /><html:hidden property="comment" value="xxx" />
								</html:form>
							</td>
							<td width="20">
								<html:form action="/ProducDeleteAction" method="post">
									<html:image page="/buttom/delete_b.gif" />
									<html:hidden property="productid" value='<%= Integer.toString(productdata.getProductid()) %>' />
									<html:hidden property="name" value="xxx" /><html:hidden property="way" value="xxx" /><html:hidden property="management" value="xxx" /><html:hidden property="comment" value="xxx" />
								</html:form>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>