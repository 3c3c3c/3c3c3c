<%@ page language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="java.util.ArrayList, java.text.NumberFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inquiry About</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


<%
	ArrayList<String> array = (ArrayList<String>)request.getAttribute("inquiry");
	ArrayList<Integer> result = (ArrayList<Integer>)request.getAttribute("result");
	double sum = 0;
	for(int i=0 ; i<result.size() ; i++){
		sum += result.get(i);
	}
	NumberFormat format = NumberFormat.getInstance();
	format.setMaximumFractionDigits(2);
%>

		<div id="boxB">
			<div id="bdr1">
				<p>
					<font><%out.print(array.get(0)); %></font>
				</p>
				<html:form action="/ReturnInquirySelectShowAction" method="post">
				<div style="float: left; width: 40%;">
					<ul>
						<%  for(int i=0 ; i<result.size() ; i++){ %>
							<li><font><%out.print(array.get(i+1)); %> : <%out.print( format.format( result.get(i)/sum * 100) ); %>%</font></li>
						<% } %>
					</ul>
				</div>

				<div style="float: right; width: 60%">
					<html:img src='<%= "http://chart.apis.google.com/chart?cht=p&amp;"+
						"chd=t:"+ result.get(0) +","+ result.get(1) +","+ result.get(2) +","+ result.get(3) +","+ result.get(4) +"&amp;"+
							"chs=600x200&amp;"+
								"chl="+ array.get(1) +"|"+ array.get(2) +"|"+ array.get(3) +"|"+ array.get(4) +"|"+ array.get(5) +"&amp;"+
									"chco=CC0000|CCCC00|00CC00|0000CC|00CCCC&amp;"+
										"chf=bg,s,ffffff00&amp;"+
											"chp=4.712" %>'
							width="600" height="200" alt='<%= array.get(0) %>' />
					<!-- <img src="chart.png" width="600" height="200">
 -->				</div>

				<div align="center" style="clear: left">
					<html:link page = "/admin/inquiryChoice.jsp"><html:img page="/buttom/back_b.gif"
					 alt="TAG index" border="0" /></html:link>
				</div>
				</html:form>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>