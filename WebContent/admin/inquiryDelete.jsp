<%@ page language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inquiry</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<%
	ArrayList<String> array = (ArrayList<String>)request.getAttribute("inquiry");
	ArrayList<Integer> num = (ArrayList<Integer>)request.getAttribute("num");
%>
		<div id="boxB">
			<div id="contents">
				<div id="bdr1">
					<p>
						<font> 削除するアンケートを選択してください </font>
					</p>
					<html:form action="/iquiryDeleteChoiceAction" method="post">
						<ol style="list-style-type: none;">
						<% for(int i = 0; i < array.size();i++) { %>
							<li><html:radio property="inquiryid" value='<%= Integer.toString(num.get(i)) %>' /><font><% out.print(array.get(i)); %></font></li>
						<% } %>
						</ol>

						<div align="center">
							<html:image page="/buttom/delete_b.gif" alt="TAG index" border="0" />
						</div>

					</html:form>

					<div align="center">
						<html:link page="/admin/inquiryChoice.jsp">
							<html:img page="/buttom/back_b.gif" alt="TAG index" border="0" />
						</html:link>
					</div>
				</div>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>