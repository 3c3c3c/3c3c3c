<%@page import="jp.co.ccc.admin.form.ProductForm"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% ArrayList<ProductForm> productdata = (ArrayList<ProductForm>)request.getAttribute("productdata"); %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Item Delete</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">
			<div id="bdr2">
			<% for(int i = 0; i < productdata.size(); i++){ %>
				<html:form action="/ProducDeleteConfirmAction" method="post">

				<table>
					<tr class="dsh">
						<td rowspan="4"><img src="photo/<%=productdata.get(i).getImage() %>"
							height="200" width="200" style="margin-right: 10px;"></td>
						<td>ID</td>
						<td><%=productdata.get(i).getProductid() %></td>
					</tr>
					<tr class="dsh">
						<td>商品名</td>
						<td><%=productdata.get(i).getName() %>
						</td>
					</tr>
					<tr class="dsh">
						<td>販売価格</td>
						<td><%=productdata.get(i).getPrice() %>円</td>
					</tr>
					<tr class="dsh">
						<td>在庫</td>
						<td><%=productdata.get(i).getStock() %>点</td>
					</tr>
				</table>
				<table>
					<tr class="dsh">
						<td>仕様</td>
						<td><%=productdata.get(i).getWay() %>
						</td>
					</tr>
					<tr class="dsh">
						<td>取扱い</td>
						<td><%=productdata.get(i).getManagement() %></td>
					</tr>
					<tr class="dsh">
						<td>コメント</td>
						<td><%=productdata.get(i).getComment() %></td>
					</tr>
				</table>
				<br>

				<div align="center">
					<font style="color:red">本当に削除してもよろしいですか</font>
				<table><tr><td></td><td>
						<html:image src="buttom/delete_b.gif" property="priductid"  />
						<html:hidden property="productid"  />
						<html:hidden property="name" value='<%=productdata.get(i).getName() %>' />
						<html:hidden property="price" value='<%=Integer.toString(productdata.get(i).getPrice()) %>' />
						<html:hidden property="stock" value='<%=Integer.toString(productdata.get(i).getStock()) %>' />
						<html:hidden property="way" value='<%=productdata.get(i).getWay() %>' />
						<html:hidden property="management" value='<%=productdata.get(i).getManagement() %>' />
						<html:hidden property="comment" value='<%=productdata.get(i).getComment() %>' />
					</td><td>
					<html:link href="admin/home.jsp"><img src="buttom/cancel_b.gif" alt="TAG index" border="0"></html:link>
				</td><td></td></tr></table>
				</div>
			</html:form>

			<%} %>


			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>