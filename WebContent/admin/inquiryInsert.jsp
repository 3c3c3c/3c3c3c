<%@ page language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Item Update</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">

			<div id="bdr1">
				<html:errors />

				<html:form action="/iquiryInsertAction" method="post">
					<table>
						<tr class="dsh">
							<center>
								<font>項目を入力してください</font>
							</center>
							<td><font>質問内容</font></td>
							<td>→</td>
							<td><html:text property="inquiry" value=""
							style="font-size: 20px;" size="70" /></td>
						</tr>
						<tr>
							<td><font>選択肢1</font></td>
							<td>→</td>
							<td><html:text property="choice1" value=""
								style="font-size: 20px;" size="70" /></td>
						</tr>
						<tr>
							<td><font>選択肢2</font></td>
							<td>→</td>
							<td><html:text property="choice2" value=""
								style="font-size: 20px;" size="70" /></td>
						</tr>
						<tr>
							<td><font>選択肢3</font></td>
							<td>→</td>
							<td><html:text property="choice3" value=""
								style="font-size: 20px;" size="70" /></td>
						</tr>
						<tr>
							<td><font>選択肢4</font></td>
							<td>→</td>
							<td><html:text property="choice4" value=""
								style="font-size: 20px;" size="70" /></td>
						</tr>
						<tr>
							<td><font>選択肢5</font></td>
							<td>→</td>
							<td><html:text property="choice5" value=""
								style="font-size: 20px;" size="70" /></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>

					<div align="center">
						<input type="hidden" name="id" value="1001">
						<html:image src="../buttom/kakunin.gif" alt="TAG index" border="0" />
					</div>

					<div align="center">
						<a href="inquiryChoice.jsp"><img src="../buttom/back.gif"
							alt="TAG index" border="0"></a>
					</div>
				</html:form>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>
