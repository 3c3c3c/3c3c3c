<%@page import="jp.co.ccc.user.form.SearchForm"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%ArrayList<SearchForm> cateResult = (ArrayList<SearchForm>) request.getAttribute("cateResult");%>
<% String keyword = (String)request.getAttribute("keyword"); %>
<% String cate = (String)request.getAttribute("cate"); %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">
	<div style="border-style: solid; border-width: 4px; padding: 10px 5px 10px 20px; border-color: red; color: green; background-color: white; width: auto; height: 30px;">
					<font size="5" color="red"> 該当商品</font>
				</div>

				<div id="bdr4" align="center">

					<%
						if (cateResult != null) {
							int count = 0;
					%>
					<table>
						<tr>
					<% for (int i = 0; i < cateResult.size(); i++) { %>


							<td><%=cateResult.get(i).getProductname()%><br>


								<html:form method="post" action="/ProductAction">
									<html:img page='<%= "/photo/"+ cateResult.get(i).getProductimage()%>'
										style="width: 180px; height: 200px;" alt="" /><br> ￥<%=cateResult.get(i).getPrice()%>(税込)<br>
									<html:image page="/buttom/change_b.gif" property="submit" />

									<html:hidden property="productid" value='<%= Integer.toString(cateResult.get(i).getProductid())%>' />
									<html:hidden property="name" value="xxx" /><html:hidden property="way" value="xxx" /><html:hidden property="management" value="xxx" /><html:hidden property="comment" value="xxx" />

								</html:form>
							</td>
					<%count++;
						if(count%3==0){%>		</tr><tr>
					<%	}
						}
					%>
						</tr>
					</table>
					<%
						}
					%>
					<div align="center">
						<center>
						<html:form method="post" action="/ProductSearchAction">
							<html:image page="/buttom/pgup_b.gif" property="submit" />
							<html:hidden property="keyword" value="<%=keyword %>" />
							<html:hidden property="cate" value="<%=cate %>" />
						</html:form>
						</center>
					</div>
				</div>
		</div>
				<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>
