<%@ page language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inquiry</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">
			<div id="bdr1">
				<p></p>
				<div align="center">
					<html:form action="/iquirySelectAction" method="post">
						<html:image page="/buttom/result.gif" alt="TAG index" border="0" />
						<html:hidden property="inquiry" value='inquiry' />
						<html:hidden property="choice1" value='choice1' />
						<html:hidden property="choice2" value='choice2' />
					</html:form>
				</div>

				<div align="center" style="margin-top: 15px;">
					<html:form action="/iquiryUpdateAction" method="post">
						<html:image page="/buttom/naiyo.gif" alt="TAG index" border="0" />
						<html:hidden property="inquiry" value='inquiry' />
						<html:hidden property="choice1" value='choice1' />
						<html:hidden property="choice2" value='choice2' />
					</html:form>
				</div>

				<div align="center" style="margin-top: 15px;">
						<html:link page="/admin/inquiryInsert.jsp"><html:img page="/buttom/newinsert.gif"
						alt="TAG index" border="0" /></html:link>
				</div>

				<div align="center" style="margin-top: 15px;">
					<html:form action="/iquiryDeleteAction" method="post">
						<html:image page="/buttom/delete_b.gif" alt="TAG index" border="0" />
						<html:hidden property="inquiry" value='inquiry' />
						<html:hidden property="choice1" value='choice1' />
						<html:hidden property="choice2" value='choice2' />
					</html:form>
				</div>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>
