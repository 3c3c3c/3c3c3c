<%@ page language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Item Update</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<%
			ArrayList<String> array = (ArrayList<String>) request.getAttribute("inquiry");
				String num = (String) request.getAttribute("num");
		%>
		<div id="boxB">
			<div id="contents">
				<div id="bdr1">
					<p>
						<font> これでよろしいですか？ </font>
					</p>
					<html:form action="/iquiryInsertConfirmAction" method="post">
						<ol style="list-style-type: none;">
							<li><font>
									<%out.print(array.get(0));%>
							</font></li>
							<li><html:radio property="choice1" value="1" disabled="true" /><font>
									<%
										out.print(array.get(1));
									%>
							</font></li>
							<li><html:radio property="choice2" value="2" disabled="true" /><font>
									<%
										out.print(array.get(2));
									%>
							</font></li>
							<li><html:radio property="choice3" value="3" disabled="true" /><font>
									<%
										out.print(array.get(3));
									%>
							</font></li>
							<li><html:radio property="choice4" value="4" disabled="true" /><font>
									<%
										out.print(array.get(4));
									%>
							</font></li>
							<li><html:radio property="choice5" value="5" disabled="true" /><font>
									<%
										out.print(array.get(5));
									%>
							</font></li>
						</ol>

						<html:hidden property="inquiry" value='<%=array.get(0)%>' />
						<html:hidden property="choice1" value='<%=array.get(1)%>' />
						<html:hidden property="choice2" value='<%=array.get(2)%>' />
						<html:hidden property="choice3" value='<%=array.get(3)%>' />
						<html:hidden property="choice4" value='<%=array.get(4)%>' />
						<html:hidden property="choice5" value='<%=array.get(5)%>' />
						<html:hidden property="num" value='<%=num%>' />

						<div align="center">
						<input type="hidden" name="id" value="1001">
						<html:image src="/3C/buttom/kettei.gif"
							alt="TAG index" border="0" />
					</div>

						<div align="center">
							<html:link page="/admin/inquiryChoice.jsp">
								<html:img page="/buttom/back_b.gif" alt="TAG index" border="0" />
							</html:link>
						</div>
					</html:form>
				</div>
			</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>