<%@page import="jp.co.ccc.admin.form.ProductForm"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% ArrayList<ProductForm> productdata = (ArrayList<ProductForm>)request.getAttribute("productdata"); %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Item</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">
			<div id="bdr2">
			<html:form action="/ProductUpdateInputAction" method="post">
				<b style="color:red;font-size:20px;"><html:errors/></b>
				<table>
					<tr class="dsh">
						<td rowspan="5"><html:img page='<%= "/photo/"+ productdata.get(0).getImage() %>'
							height="200" width="200" style="margin-right: 10px;" /></td>
						<td>ID</td>
						<td><%=productdata.get(0).getProductid() %></td>
					</tr>
					<tr class="dsh">
						<td>商品名</td>
						<td><html:text property="name" value='<%= productdata.get(0).getName() %>'
								style="width: 200px; height: 30px;"/></td>
					</tr>
					<tr class="dsh">
						<td>ジャンル</td>
						<td>
							<html:select property="genre" value='<%= Integer.toString(productdata.get(0).getGenre()) %>'>
								<html:option value="1">ボーダー柄</html:option>
								<html:option value="2">柄物</html:option>
								<html:option value="3">その他</html:option>
								<html:option value="4">ハーフパンツ</html:option>
								<html:option value="5">ショートパンツ</html:option>
								<html:option value="6">長ズボン</html:option>
								<html:option value="7">コート</html:option>
								<html:option value="8">ジャケット</html:option>
								<html:option value="9">ベスト</html:option>
								<html:option value="10">キャップ</html:option>
								<html:option value="11">ハット</html:option>
								<html:option value="12">ニット帽</html:option>
								<html:option value="13">スニーカー</html:option>
								<html:option value="14">サンダル</html:option>
								<html:option value="15">長ぐつ</html:option>
							</html:select>
						</td>
					</tr>
					<tr class="dsh">
						<td>販売価格</td>
						<td><html:text property="price" value='<%= Integer.toString(productdata.get(0).getPrice()) %>'
								style="width: 100px; height: 30px;"/>円</td>
					</tr>
					<tr class="dsh">
						<td>在庫</td>
						<td><html:text property="stock" value='<%= Integer.toString(productdata.get(0).getStock()) %>' style="width: 100px" />点</td>
					</tr>
				</table>
				<table>
					<tr class="dsh">
						<td>仕様</td>
						<td><html:textarea property="way" value='<%= productdata.get(0).getWay() %>'
								cols="40" rows="5"/></td>
					</tr>
					<tr class="dsh">
						<td>取扱い</td>
						<td><html:textarea property="management" value='<%= productdata.get(0).getManagement() %>'
								cols="40" rows="5"/></td>
					</tr>
					<tr class="dsh">
						<td>コメント</td>
						<td><html:textarea property="comment" value='<%= productdata.get(0).getComment() %>'
								cols="40" rows="5"/></td>
					</tr>
				</table>

				<table><tr><td></td><td>
				<html:image page="/buttom/update_b.gif" />
				<html:hidden property="productid" value='<%= Integer.toString(productdata.get(0).getProductid())%>' />
				</td><td>
					<html:link page="/admin/home.jsp"><html:img page="/buttom/cancel_b.gif" alt="TAG index" border="0" /></html:link>
				</td><td></td></tr></table>
			</html:form>
			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		</div></body>
</html:html>