<%@page import="java.sql.ResultSet"%>
<%@page import="jp.co.ccc.user.form.SearchForm"%>
<%@page import="jp.co.ccc.dao.DbUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">

			<div id="contents">
				<html:form action="/ProductSearchAction" method="post">
					<table class="select">
						<tr>
							<td><input type="text" name="keyword" value=""></td>
							<td><select name="cate">
									<option value="" selected>ジャンル選択</option>
									<option value="ボーダー柄">ボーダー柄</option>
									<option value="柄物">柄物</option>
									<option value="その他">その他</option>
									<option value="ハーフパンツ">ハーフパンツ</option>
									<option value="ショートパンツ">ショートパンツ</option>
									<option value="長ズボン">長ズボン</option>
									<option value="コート">コート</option>
									<option value="ジャケット">ジャケット</option>
									<option value="ベスト">ベスト</option>
									<option value="キャップ">キャップ</option>
									<option value="ハット">ハット</option>
									<option value="ニット帽">ニット帽</option>
									<option value="スニーカー">スニーカー</option>
									<option value="サンダル">サンダル</option>
									<option value="長ぐつ">長ぐつ</option>
								</select></td>
							<td>
					<html:image page="/buttom/search_b.gif" property="submit" value="送信"></html:image></td>
						</tr>
					</table>
				</html:form>
			</div>


			<div id="bdr4" align="center">
				<table>
				<tr>

<%
//データベース接続
DbUtil util = new DbUtil();
//入力が無い場合全検索

String sql ="SELECT * FROM PRODUCT_TABLE ORDER BY productid;";
PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(), sql);
ResultSet rs = stmt.executeQuery();
int count = 0;
while(rs.next()){
	SearchForm sf = new SearchForm();
	int productid = rs.getInt("productid");
	String name = rs.getString("productname");
	String image = rs.getString("productimage");
	int price = rs.getInt("price");
%>

				<td class="bottom">
				<div><%=productid %></div>
				<div><%=name %></div>
				<div><html:img page='<%= "/photo/"+ image %>' height="200" width="200" /></div>
				<div>

					<html:form method="post" action="/ProductAction">
						<html:image page="/buttom/change_b.gif" property="submit" />
						<html:hidden property="productid" value='<%= Integer.toString(productid)%>' />
						<html:hidden property="name" value='xxx' />
						<html:hidden property="genre" value='1' />
						<html:hidden property="price" value='1' />
						<html:hidden property="stock" value='1' />
						<html:hidden property="way" value='xxx' />
						<html:hidden property="management" value='xxx' />
						<html:hidden property="comment" value='xxx' />
					</html:form>

				</div>
				</td>
<%
	count++;
	if(count%5 == 0){
%>
				</tr><tr>
<%
	}
	if(count == 10){
		break;
	}
}
%>
				</tr>
				</table>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>
