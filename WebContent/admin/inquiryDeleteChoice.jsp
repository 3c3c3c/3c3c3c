<%@ page language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inquiry</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<%
	ArrayList<String> array = (ArrayList<String>)request.getAttribute("inquiry");
	String num = (String) request.getAttribute("num");
%>

		<div id="boxB">

			<div id="bdr1">

				<html:form action="/iquiryDeleteConfirmAction" method="post">
					<table>
						<tr class="dsh">
							<center>
								<font>do you delete?</font>
							</center>
							<td></td>
							<td><font><% out.print(array.get(0));%></font></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><font><% out.print(array.get(1)); %></font></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><font><% out.print(array.get(2)); %></font></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><font><% out.print(array.get(3)); %></font></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><font><% out.print(array.get(4)); %></font></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><font><% out.print(array.get(5)); %></font></td>
							<td></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>

					<div align="center">
						<input type="hidden" name="id" value="1001">
						<html:image page="/buttom/delete_b.gif" alt="TAG index" border="0" />
					</div>

					<html:hidden property="num" value='<%=num%>' />

					</html:form>

					<div align="center">
						<html:link page="/admin/inquiryChoice.jsp">
							<html:img page="/buttom/back_b.gif" alt="TAG index" border="0" />
						</html:link>
					</div>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>