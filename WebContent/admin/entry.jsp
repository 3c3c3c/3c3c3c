<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Entry</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">
			<div id="bdr2" align="center" style="margin-top:30px;">
<html:errors />
				<html:form action="/ProductEntryAction" method="post" enctype="multipart/form-data">
				<b style="color:red;font-size:20px;"><html:errors/></b>
					<table>
						<tr>
							<td>商品名</td>
							<td><input type="text" name="name"></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>カテゴリー</td>
							<td><select name="genre" style="font-size: 20px;">
									<option value="" selected>選択なし</option>
									<option value="1">ボーダー柄</option>
									<option value="2">柄もの</option>
									<option value="3">その他</option>
									<option value="4">ハーフパンツ</option>
									<option value="5">ショートパンツ</option>
									<option value="6">長ズボン</option>
									<option value="7">コート</option>
									<option value="8">ジャケット</option>
									<option value="9">ベスト</option>
									<option value="10">キャップ</option>
									<option value="11">ハット</option>
									<option value="12">ニット</option>
									<option value="13">スニーカー</option>
									<option value="14">サンダル</option>
									<option value="15">長ぐつ</option>
							</select></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>価格</td>
							<td><input type="text" name="price"></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>在庫数</td>
							<td><input type="text" name="stock"></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>商品画像</td>
							<td><input type="file" name="image"></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>仕様</td>
							<td><textarea type="text" name="way"
									style="width: 300px; height: 100px;"></textarea></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>取扱い</td>
							<td><textarea type="text" name="management"
									style="width: 300px; height: 30px;"></textarea></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>コメント</td>
							<td><textarea name="comment"
									style="width: 300px; height: 100px;"></textarea></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
					</table>
					<html:image page="/buttom/confirm_b.gif" property="submit" value="送信"></html:image>

				</html:form>


			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>
