<%@page import="jp.co.ccc.admin.form.ProductEntryForm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String name = (String)request.getAttribute("name"); %>
<% int genre = (Integer)request.getAttribute("genre"); %>
<% int price = (Integer)request.getAttribute("price"); %>
<% int stock = (Integer)request.getAttribute("stock"); %>
<% String image = (String)request.getAttribute("image"); %>
<% String way = (String)request.getAttribute("way"); %>
<% String management = (String)request.getAttribute("management"); %>
<% String comment = (String)request.getAttribute("comment"); %>

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">

			<div id="bdr2" align="center">
				<table>
					<tr class="dsh">
						<td rowspan="4">
							<html:img page='<%= "/photo/"+ image %>' width="200px" height="200px" />
						</td>
					</tr>
					<tr class="dsh">
						<td>商品名</td>
						<td><bean:write name="name" /></td>
					</tr>
					<tr class="dsh">
						<td>販売価格</td>
						<td><bean:write name="price" />円</td>
					</tr>
					<tr class="dsh">
						<td>在庫</td>
						<td><bean:write name="stock" />点</td>
					</tr>
					<tr>
						<td >仕様</td>
						<td width="200px" colspan="2"><bean:write name ="way" /></td>
					</tr>
					<tr>
						<td>取扱い</td>
						<td width="200px" colspan="2"><bean:write name ="management" /></td>
					</tr>
					<tr>
						<td>コメント</td>
						<td width="200px" colspan="2"><bean:write name ="comment" /></td>
					</tr>

				</table>

				<html:form action="/ProductEntryConfirmAction" method="post">
					<table>
						<tr>
							<td></td>
							<td style="color: red;"></td>
						</tr>
					</table>

					<html:hidden property = "name" value = '<%=name %>'  />
					<html:hidden property = "genre" value = '<%= Integer.toString(genre) %>'  />
					<html:hidden property = "price" value = '<%= Integer.toString(price) %>'  />
					<html:hidden property = "stock" value = '<%= Integer.toString(stock) %>'  />
					<html:hidden property = "imagename" value = '<%=image %>'  />
					<html:hidden property = "way" value = '<%=way %>'  />
					<html:hidden property = "management" value = '<%=management %>'  />
					<html:hidden property = "comment" value = '<%=comment %>'  />

					<html:image page="/buttom/decide.gif" />
					<html:link page="/admin/entry.jsp"><html:img page="/buttom/cancel_b.gif" alt="キャンセル"/></html:link>
				</html:form>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>
