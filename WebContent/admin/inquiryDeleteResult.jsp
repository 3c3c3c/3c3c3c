<%@ page language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Delete</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<%
	int id = (Integer)request.getAttribute("id");
%>

		<div id="boxB">

			<div align="center">
				<div id="bdr1">
					<div>
						<font>ID : <% out.print(id);%><br>
						のアンケートを削除しました。</font>
					</div>

					<html:link page="/admin/home.jsp"><html:img page="/buttom/top_b.gif"
						alt="TAG index" border="0" /></html:link>
				</div>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>