<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@page import="jp.co.ccc.user.form.UserInfoForm"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% ArrayList<UserInfoForm> usertable = (ArrayList<UserInfoForm>) request.getAttribute("usertable"); %>
<% request.setCharacterEncoding("UTF-8"); %>

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User</title>
<LINK rel="stylesheet" type="text/css"
	href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css"
	href="<html:rewrite page="/css/layout.css"/>">
<script type="text/javascript" src="js/default.js"></script>


<body>
	<script src="http://www.google.com/jsapi"></script>
	<script>
		google.load("jquery", "1.7");
		google.load("jqueryui", "1.8");
	</script>
	<div id="container">
		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">
			<div id="contents">
				<html:form action="userSearchAction" method="post">
					<br>
					<table class="select">
						<tr>
							<td><html:text property="keyword" value="" /></td>
							<td><html:select property="cate" value="">
									<option value="ID" selected>ID</option>
									<option value="氏名">氏名</option>
									<option value="住所">住所</option>
									<option value="電話番号">電話番号</option>
									<option value="メールアドレス">メールアドレス</option>
								</html:select></td>
							<td><html:image page="/buttom/search_b.gif" property="submit" alt="TAG index" border="0" /></td>
						</tr>
					</table>
				</html:form>
			</div>

			<div align="center">
				<html:form action="userDeleteAction" method="post">

					<table border="1" align="center">
						<tr bgcolor="#F0F0F0">
							<td>ID</td>
							<td>氏名</td>
							<td>ふりがな</td>
							<td>住所</td>
							<td>電話番号</td>
							<td>メールアドレス</td>
							<td>
								削除

							</td>
						</tr>
						<% for (int i = 0; i < usertable.size(); i++) { %>
						<tr>
							<td>
								<% out.println(usertable.get(i).getUserid()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getUsername_family() + usertable.get(i).getUsername_first()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getUsername_family_ruby() + usertable.get(i).getUsername_first_ruby()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getAdr_prefectural() + usertable.get(i).getAdr_city() + usertable.get(i).getAdr_town()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getTel()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getMail()); %>
							</td>

							<td align="center">
								<html:multibox property="delete" value='<%= Integer.toString(usertable.get(i).getUserid()) %>' />
							</td>
						</tr>
						<% } %>
					</table>


					<table align="center">
						<tr>
							<td align="center">
								<html:image page="/buttom/delete_b.gif" alt="TAG index" border="0" />
							</td>
							<td align="center">
								<button type="reset" style="border-style: none;" onClick="javascript:location.reload()">
									<html:img page="/buttom/selectcancel.gif" alt="TAG index" border="0" />
								</button>
							</td>
						</tr>
					</table>
				</html:form>
			</div>

		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>
