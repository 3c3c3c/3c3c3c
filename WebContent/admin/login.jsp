<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%request.setCharacterEncoding("UTF-8"); %>
<%String er = (String)request.getAttribute("e"); %>

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Login</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>

	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/head.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<div id="boxB">

<center><font color = "red"><% if(er!=null){
	out.println(er);
	} %></font></center>
			<div id="bdr2" align="center">
				<html:form action="/adminAction" method="POST">
					<table>
						<tr>
							<td><span style="color: red">※</span>ID</td>
							<td><html:text property="adminid" value="${param.id}"/></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><span style="color: red">※</span>PASS</td>
							<td><html:password property="pass" value="${param.pass}"/></td>
						</tr>
						<tr class="dsh">
							<td></td>
							<td></td>
						</tr>
					</table>
					<br>
					<html:image page="/buttom/login_b2.gif" alt="TAG index" border="0"/>
					<button type="reset" style="border-style: none;" ><html:img page="/buttom/reset_b.gif" alt="TAG index" border="0"/></button>
				</html:form>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>