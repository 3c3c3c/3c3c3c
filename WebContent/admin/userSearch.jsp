<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@page import="jp.co.ccc.admin.form.UserForm"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%ArrayList<UserForm> usertable  = (ArrayList<UserForm>)request.getAttribute("usertable"); %>
<%request.setCharacterEncoding("UTF-8"); %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
<script type="text/javascript" src="js/default.js"></script>
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>

<% for(int i=0; i<usertable.size();i++){ %>
<%int userid = (usertable.get(i)).getUserid(); %>
<%String username_family = (usertable.get(i)).getUsername_family(); %>
<%String username_first = (usertable.get(i)).getUsername_first(); %>
<%String username_family_ruby = (usertable.get(i)).getUsername_family_ruby(); %>
<%String username_first_ruby = (usertable.get(i)).getUsername_first_ruby(); %>
<%String adr_prefectural = (usertable.get(i)).getAdr_prefectural(); %>
<%String adr_city = (usertable.get(i)).getAdr_city(); %>
<%String adr_town = (usertable.get(i)).getAdr_town(); %>
<%String adr_location = (usertable.get(i)).getAdr_location(); %>
<%String tel = (usertable.get(i)).getTel(); %>
<%String mail = (usertable.get(i)).getMail(); %>
<% }%>

		<div id="boxB">
			<div id="contents">
				<html:form action="/userSearchAction" method="post">
					<br><table class="select">
						<tr>
							<td><html:text property="keyword" value=""/></td>
							<td><html:select property="cate" value="">
									<option value="id" selected>ID</option>
									<option value="name">氏名</option>
									<option value="address">住所</option>
									<option value="tel">電話番号</option>
									<option value="mail">メールアドレス</option>
							</html:select></td>
							<td>
						<html:image src="/3C/buttom/search_b.gif"  property="submit" alt="TAG index" border="0"></html:image></td>
						</tr>
					</table>
				</html:form>
			</div>

			<div align="center">
						<html:form action="/userDeleteAction" method="post" >
				<table >
					<tr>
					<td colspan="2" align="center">
					<table border="1">
						<tr bgcolor="#F0F0F0">
							<td>ID</td>
							<td>氏名</td>
							<td>ふりがな</td>
							<td>住所</td>
							<td>電話番号</td>
							<td>メールアドレス</td>
							<td>削除</td>
						</tr>
			<%for(int i=0; i<usertable.size();i++){ %>
						<tr>
									<td><%out.println(usertable.get(i).getUserid());%></td>
									<td><%out.println(usertable.get(i).getUsername_family()); %><%out.println(usertable.get(i).getUsername_first()); %></td>
									<td><%out.println(usertable.get(i).getUsername_family_ruby()); %><%out.println(usertable.get(i).getUsername_first_ruby()); %></td>
									<td><%out.println(usertable.get(i).getAdr_prefectural()); %><%out.println(usertable.get(i).getAdr_city()); %><%out.println(usertable.get(i).getAdr_town()); %></td>
									<td><%out.println(usertable.get(i).getTel()); %></td>
									<td><%out.println(usertable.get(i).getMail()); %></td>

									<td>
									<center><html:checkbox property="delete" value='<%= Integer.toString(usertable.get(i).getUserid()) %>' /></center>

									</td>

								</tr>
	<% }%>
							</table>
						</td>
					</tr>
				</table>
				<html:image page="/buttom/delete_b.gif" alt="TAG index" border="0"/>
				</html:form>
			</div>

		</div>









<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>