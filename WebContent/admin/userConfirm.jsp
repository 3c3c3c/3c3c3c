<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@page import="jp.co.ccc.admin.form.UserForm"%>
<% ArrayList<UserForm> usertable = (ArrayList<UserForm>) request.getAttribute("usertable"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User Confirm</title>
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/style.css"/>">
<LINK rel="stylesheet" type="text/css" href="<html:rewrite page="/css/layout.css"/>">
</head>
<body>
	<div id="container">

		<!----- ヘッダ -------------------------------------------------->
		<div id="boxA">
			<jsp:include page="/admin/header.jsp" />
			<jsp:include page="/admin/menu.jsp" />
		</div>
		<!----- ヘッダ -------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>

		<div id="boxB">
			<div align="center">
				<html:form action="/userDeleteConfirmAction" method="post">
					<table border="1" align="center">
						<tr bgcolor="#F0F0F0">
							<td>ID</td>
							<td>氏名</td>
							<td>ふりがな</td>
							<td>住所</td>
							<td>電話番号</td>
							<td>メールアドレス</td>
						</tr>
						<% for (int i = 0; i < usertable.size(); i++) { %>
						<tr>
							<td>
								<% out.println(usertable.get(i).getUserid()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getUsername_family() + usertable.get(i).getUsername_first()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getUsername_family_ruby() + usertable.get(i).getUsername_first_ruby()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getAdr_prefectural() + usertable.get(i).getAdr_city() + usertable.get(i).getAdr_town()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getTel()); %>
							</td>
							<td>
								<% out.println(usertable.get(i).getMail()); %>
							</td>

							<html:hidden property="delete" value='<%= Integer.toString(usertable.get(i).getUserid()) %>' />
						</tr>
						<% } %>
					</table>


					<table align="center">
						<tr>
							<td align="center">
								<html:image page="/buttom/delete_b.gif" alt="TAG index" border="0" />
							</td>
							<td align="center">
								<html:link action="/userAction" >
									<html:img page="/buttom/cancel_b.gif" />
								</html:link>
							</td>
						</tr>
					</table>
				</html:form>
			</div>
		</div>
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	</div>
</body>
</html:html>