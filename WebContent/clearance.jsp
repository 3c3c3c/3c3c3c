<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="jp.co.ccc.user.form.ProductForm"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="java.util.ArrayList"%>
<%
	ArrayList<ProductForm> saleInfo = (ArrayList<ProductForm>) request
			.getAttribute("saleInfo");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
<LINK rel="stylesheet" type="text/css" href="timesale.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div
					style="border-style: solid; border-width: 4px; padding: 10px 5px 10px 20px; border-color: red; color: green; background-color: white; width: auto; height: 30px;">
					<font size="5" color="red"> 商品一覧</font>
				</div>
				<br>


				<div id="bdr4" align="center">
					<table>
						<tr>
							<%
								int count = 0;
								for (int i = 0; i < saleInfo.size(); i++) {
							%>

							<td><%=saleInfo.get(i).getProductname()%><br>

								<html:form method="post" action="/detailAction">
									<input type="image"
										src="photo/<%=saleInfo.get(i).getProductimage()%>" width="180"
										height="200" name="button" alt="" /><br> ￥<%=saleInfo.get(i).getPrice()%>(税込)<br>
										<%int price = saleInfo.get(i).getPrice();
										int half = price/2;%>
										<b style="color:red;"> \<%=half%>(50%OFF)</b>
										<html:hidden property="productid" value='<%=Integer.toString(saleInfo.get(i).getProductid())%>' />

								</html:form></td>
							<%
								count++;

									if (count % 3 == 0) {
							%>
						</tr>
						<tr>
							<%
								}
								}
							%>
						</tr>
					</table>
				</div>

			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html>