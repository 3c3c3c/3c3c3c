<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
		<font>
<table rules="rows" border="1" width="100%">
	<tr>
		<td align="center">
				<img src="photo/shoes/shoes1.jpg" width="400" height="400"/>
		</td>
		<td align="center">
			<img src="photo/shoes/shoes2.jpg" width="400" height="400">
		</td>
		<td align="center">
			<img src="photo/shoes/shoes3.jpg" width="400" height="400">
		</td>
	</tr>

	<tr  bgcolor="#FFEBCD">
		<td align="center">中敷き付きレインシューズ（女の子）<br>￥1,077(税込)</td>
		<td align="center">華やか＆厚底♪女の子ビーチサンダル<br>￥980(税込)</td>
		<td align="center">１～３歳児向け☆笛底サンダル<br>￥1,058(税込)</td>
	</tr>

	<tr>
			<td align="center"><img src="photo/shoes/shoes4.jpg" width="400" height="400"/></td>
			<td align="center"><img src="photo/shoes/shoes5.jpg" width="400" height="400"/></td>
			<td align="center"><img src="photo/shoes/shoes7.jpg" width="400" height="400"/></td>
		</tr>

		<tr  bgcolor="#FFEBCD">
			<td align="center">歩き始めのベビーに♪ソフトシューズ<br>￥1,169(税込)
			</td>
			<td align="center">２～４歳向け☆女児サンダル<br>￥880(税込)
			</td>
			<td align="center">ベビースリッポン<br>\980(税込)
			</td>
		</tr>

	<tr>
			<td align="center"><img src="photo/shoes/shoes8.jpg" width="400" height="400"></td>
			<td align="center"><img src="photo/shoes/shoes9.jpg" width="400" height="400"></td>
			<td align="center"><img src="photo/shoes/shoes10.jpg" width="400" height="400"></td>
		</tr>

		<tr  bgcolor="#FFEBCD">
			<td align="center">２～４歳向け☆男児笛底デザインシューズ<br>￥1,058(税込)
			</td>
			<td align="center">２～４歳向け☆男女児デザインシューズ<br>￥1,252(税込)
			</td>
			<td align="center">ベビースニーカー<br>\2.058(税込)
			</td>
		</tr>


		<tr>
			<td align="center"><img src="photo/shoes/shoes6.jpg" width="400" height="400"></td>
		</tr>
		<tr  bgcolor="#FFEBCD">
			<td align="center">アニマル下駄（ブタ）<br>￥1,069(税込)
		</td>
		</tr>

	</table>
	<div align = "center">
<center><a href="shoes.html"><img src="buttom/pgup_b.gif" alt="TAG index" border="0"></a></center>
 </div>
 </font>
			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html>