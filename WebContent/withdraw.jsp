<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
    <%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
    <%@page import= "java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<% String error = (String)request.getAttribute("wderror"); %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<p style="margin: 20px;">
						<font>======================お客様情報入力=======================</font>
					</p>

					<p style="margin: 20px;">
						<span><b style="font-size: 28px;"> 退会処理を行います。<br>
								メールドレス・PASSを入力し<br>
								よろしければ退会を押下してください。
					</b></span></p>
<% if(error != null){ %>
	<font size="20px" color ="red"><% out.print(error); %></font>
<% } %>
					<html:form action="WithdrawAction" method="post">
					<table border="1">
					<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>メールアドレス</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="mail"
									style="width: 570px; height: 30px" /></td>
									<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>パスワード</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:password property="pass"
									style="width: 150px; height: 30px" /></td>
									</table>
									<p style="margin: 15px;">
							<b style="font-size: 24px;"><span style="color: red">※</span>は必須項目です</b>
							<p style="margin: 15px;">
							<a href="withdrawConf.jsp"> <html:image src="buttom/withdraw_b.gif" alt="TAG index" border="0" /> </a>
							</tr>
						</b></span>
					</html:form>
				</div>
			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>