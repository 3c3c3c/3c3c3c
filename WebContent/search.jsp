<%@page import="jp.co.ccc.user.form.SearchForm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="java.util.ArrayList"%>
<%ArrayList<SearchForm> iteminfo = (ArrayList<SearchForm>) request.getAttribute("iteminfo");%>
<%ArrayList<SearchForm> cateResult = (ArrayList<SearchForm>) request.getAttribute("cateResult");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
<LINK rel="stylesheet" type="text/css" href="timesale.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB" align="center">

				<div
					style="border-style: solid; border-width: 4px; padding: 10px 5px 10px 20px; border-color: red; color: green; background-color: white; width: auto; height: 30px;">
					<font size="5" color="red"> 該当商品</font>
				</div>

				<div id="bdr4" align="center">
					<%
						if (iteminfo != null) {
							int count = 0;
					%>
					<table>
						<tr>
					<%
						for (int i = 0; i < iteminfo.size(); i++) {
					%>

							<td><%=iteminfo.get(i).getProductname()%><br>

								<html:form method="post" action="/detailAction">
									<input type="image"src="photo/<%=iteminfo.get(i).getProductimage()%>" alt=""height="200px" width="200px" style="margin-right: 10px;"/><br>
									￥<%=iteminfo.get(i).getPrice()%>(税込)
									<html:hidden property="productid" value='<%=Integer.toString(iteminfo.get(i).getProductid())%>' />
								</html:form></td>
					<%
							count++;

							if(count%3==0){
					%>			</tr><tr>
					<%		}
						}
					%>
						</tr>
					</table>
					<%
						} else if (cateResult != null) {
							int count = 0;
					%>
					<table>
						<tr>
					<%
						for (int i = 0; i < cateResult.size(); i++) {
					%>
							<td><%=cateResult.get(i).getProductname()%><br>
								<html:form method="post" action="/detailAction">
									<input type="image"src="photo/<%=cateResult.get(i).getProductimage()%>"
										 alt=""height="200px" width="200px" style="margin-right: 10px;" /><br> ￥<%=cateResult.get(i).getPrice()%>(税込)
									<html:hidden property="productid" value='<%=Integer.toString(cateResult.get(i).getProductid())%>' />
								</html:form></td>
					<%
						count++;
						if(count%3==0){
							%></tr><tr><%
						}
						}
					%>
						</tr>
					</table>
					<%
						}
					%>
				</div>
			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>
