<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<p style="margin: 15px;">
						<font>ログインしてください</font>
					</p>

					<html:form action="/loginAction" >
						<% String str = "";
							if( session.getAttribute("path") != null ){
								if( ((String)session.getAttribute("path")).equals("buy") ){
									str = "buy";
								}
								else{
									str = "login";
								}
							}
						%>
						<input type="hidden" name="path" value='<%= str %>'>
						<b style="color:red;font-size:20px;"><html:errors/></b>
						<table border="1" style="width: 700px;">
							<tr>
								<td align="center" style="padding: 20px; background-color: #CCFF99;">
									<b><span style="color: red">※</span>メールアドレス:</b>
								</td>
								<td style="padding: 20px; background-color: white;">
									<html:text property="mail" size="70" />
								</td>
							</tr>
							<tr>
								<td align="center" style="padding: 20px; background-color: #CCFF99;">
									<b><span style="color: red">※</span>パスワード:</b>
								</td>
								<td style="padding: 20px; background-color: white;">
									<html:password property="pass" size="70" />
								</td>
							</tr>
						</table>

						<p style="margin: 15px;">
							<span><b style="color: red; font-size: 25px;">※全て必須項目です。</b></span>
						</p>

						<p style="margin: 15px;">
							<html:image page="/buttom/login_b.gif" alt="TAG index" border="0" />
							<html:link page="/makeuser.jsp"> <html:img page="/buttom/makeuser.gif" alt="TAG index" border="0" /> </html:link>
						</p>
						<p style="margin: 15px;">
							<font>パスワードを忘れた方は<html:link page="/rePassword.jsp">こちら</html:link></font>
						</p>
					</html:form>
				</div>
			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>