<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import = "java.util.ArrayList,jp.co.ccc.entity.InquirytableEntity" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
<LINK rel="stylesheet" type="text/css" href="timesale.css">
<style type="text/css">
<!--
    #border{
		width: 700px;
		font-size: 20px;
		padding: 10px;
		border: inset 2px #DC143C;
		background-color: #ffdead;
		-webkit-border-radius: 40px;
		-moz-border-radius: 40px;
		border-radius: 40px;
		box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.4);
	}
	#border input[type="radio"]{
		margin-left: 15px;
	}
-->
</style>

</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<p style="margin: 20px;">
						<html:img page="/img/process05.png" width="900px" height="120px" />
					</p>

					<html:form action="/inquiryReplyAction" method="post">
						<div id="border">
							<table border="0">
								<tr>
									<td align="center">
										<p style="margin: 15px;">
											<font size="5"><b>～アンケート～</b></font>
										</p>
										<% String[] reply = (String[])request.getAttribute("reply"); %>
										<% ArrayList<InquirytableEntity> inquiry = (ArrayList<InquirytableEntity>)request.getAttribute("inquiry"); %>
										<logic:iterate id="data" collection="<%= inquiry %>" indexId="idx" >
											<% InquirytableEntity inquiryent = (InquirytableEntity)data; %>
											<p style="margin: 10px;"> <font size="3"><%= inquiryent.getInquiry() %></font> </p>
											<html:radio property='<%= "reply["+ idx +"]" %>' value="1" disabled="true" styleClass="check[0]"/><%= inquiryent.getChoice1() %>
											<html:radio property='<%= "reply["+ idx +"]" %>' value="2" disabled="true" styleClass="check[1]"/><%= inquiryent.getChoice2() %>
											<html:radio property='<%= "reply["+ idx +"]" %>' value="3" disabled="true" styleClass="check[2]"/><%= inquiryent.getChoice3() %>
											<html:radio property='<%= "reply["+ idx +"]" %>' value="4" disabled="true" styleClass="check[3]"/><%= inquiryent.getChoice4() %>
											<html:radio property='<%= "reply["+ idx +"]" %>' value="5" disabled="true" styleClass="check[4]"/><%= inquiryent.getChoice5() %>
											<hr style="margin: 10px;">
											<script type="text/javascript">
												<!--
												$(function(){
													$('<%= ".check["+ reply[idx] +"]" %>').attr('checked', true ); //チェックを入れる
												});
												// -->
											</script>
											<html:hidden property='<%= "reply["+ idx +"]" %>' value='<%= reply[idx] %>' />
										</logic:iterate>
									</td>
								</tr>
							</table>
						</div>
							<html:image page="/buttom/send.gif" alt="TAG index" border="0" />
							<html:image page="/buttom/back_b.gif" />
					</html:form>
				</div>
			</div>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>