<%@ page language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import= "java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%ArrayList<String> array = (ArrayList<String>)request.getAttribute("array"); %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>確認画面</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<div id="boxB">
				<div align="center">
					<p style="margin: 20px;">
						<font>======================お客様情報入力=======================</font>
					</p>

					<p style="margin: 20px;">
						<span><b style="font-size: 28px;"> 以下の内容でよろしいでしょうか。<br>
								よろしければ登録完了を押下してください。
						</b></span>
					</p>

					<html:form action="makeUserConfirmAction" method="post">
						<table border="1">
							<tr>
								<td align="center"
									style="width: 200px; font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>姓</td>
								<td style="padding-left: 5px; background-color: white;">
								<html:text property= "username_family" readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(0)); %></html:text></td>

								<td align="center"
									style="width: 200px; font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>名</td>
								<td style="padding-left: 5px; background-color: white;">

								<html:text property= "username_first" readonly="true" style="background-color:#e0e0e0">
								<% out.print(array.get(1)); %>
								</html:text>
								</td>
							</tr>
							<tr>
								<td align="center"
									style="width: 200px; font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>せい</td>
								<td style="padding-left: 5px; background-color: white;">
								<html:text property= "username_family_ruby" readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(2)); %></html:text></td>
								<td align="center"
									style="width: 200px; font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>めい</td>
								<td style="padding-left: 5px; background-color: white;">
								<html:text property= "username_first_ruby" readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(3)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>郵便番号</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:text property= "postal" readonly="true" style="background-color:#e0e0e0"><%out.print(array.get(4)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>都道府県</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:text property= "adr_prefectural"  readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(5)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>市町村</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:text property= "adr_city"  readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(6)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>番地</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:text property= "adr_town"  readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(7)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">アパート・マンション名</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:text property= "adr_location" readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(8)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>電話番号</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:text property= "tel" readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(9)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>メールアドレス</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:text property= "mail"  readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(10)); %></html:text></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>パスワード</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:password property= "pass"  readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(11)); %>"</html:password></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;"><span
									style="color: red">※</span>パスワード再入力</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;">
									<html:password property= "repass"  readonly="true" style="background-color:#e0e0e0"><% out.print(array.get(12)); %></html:password></td>
							</tr>
						</table>
						<p style="margin:15px;">
							<html:image src="buttom/touroku_b.gif" alt="TAG index" border="0" />
						</p>
					</html:form>
				</div>

			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>