<%@ page language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
<LINK rel="stylesheet" type="text/css" href="timesale.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<%
				String mail = (String)request.getAttribute("mail");
			%>

			<html:form action="/rePasswordMailAddressAction" method="post">
			<div id="boxB"align="center">
				<br> <br>
				<center>
					<img src="img/passprocess3.png" width="900px"height="100px" />
				</center>
				<br> <br>
				<center>
					<font size="4">もう一度メールアドレスを入力してください</font>
				</center>
				<b style="color:red;font-size:20px;"><html:errors /></b>
				<table border="1" cellspacing="0" bgcolor="white" width="700"
					height="100" align="center">
					<tr>
						<td style="padding: 20px; background-color: #CCFF99;"><b><center>
									<span style="color: red">※</span>メールアドレス:
								</center></b></td>
						<td style="padding: 20px; background-color: white;"><html:text property ="mail" size="70" value = ""/></td>
					</tr>
				</table>
				<br>
				<center>上記の内容でよろしければパスワードの再設定へ進むボタンを押してください</center>
				<br>

				<center>
					<html:image src="buttom/repass.gif"
						alt="TAG index" border="0" />
				</center>
				<html:hidden property="mail" value='<%=mail%>' />

			</div>
			</html:form>
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>
