<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Change Info</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<script src="http://www.google.com/jsapi"></script>
	<script>
		google.load("jquery", "1.7");
		google.load("jqueryui", "1.8");
	</script>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">
					<p style="margin: 15px;">
						<font>ユーザ情報を変更してください </font>
					</p>

					<b style="color:red;font-size:20px;"><html:errors /></b>

					<html:form action="/changeInfoAction" >
						<table border="1">
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>姓
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family" style="width: 210px; height: 30px" styleId="add1" />
								</td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>名
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first" style="width: 210px; height: 30px" styleId="add2" />
								</td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>せい</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family_ruby" style="width: 210px; height: 30px" styleId="add3" /></td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>めい</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first_ruby" style="width: 210px; height: 30px" styleId="add4" />
								</td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>郵便番号</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="postal" style="width: 210px; height: 30px" styleId="add5" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>都道府県</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_prefectural"  size="" style="width: 500px; height: 30px" styleId="add6" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>市町村</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_city" style="width: 500px; height: 30px" styleId="add7" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>番地</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_town" style="width: 500px; height: 30px" styleId="add8" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">アパート・マンション名</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_location" style="width: 500px; height: 30px" styleId="add9" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>電話番号</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="tel" style="width: 210px; height: 30px" styleId="add10" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>メールアドレス</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="mail" style="width: 500px; height: 30px" styleId="add11" /></td>
							</tr>
						</table>
						<p style="margin: 15px auto;"><b style="font-size: 24px;"><span style="color: red">※</span>は必須項目です</b></p>

						<script type="text/javascript">
						<!--
						$(function(){
						  $("#add1").attr('placeholder', '田中');
						  $("#add2").attr('placeholder', '太郎');
						  $("#add3").attr('placeholder', 'たなか');
						  $("#add4").attr('placeholder', 'たろう');
						  $("#add5").attr('placeholder', '012-0123');
						  $("#add6").attr('placeholder', '埼玉県');
						  $("#add7").attr('placeholder', '和光市');
						  $("#add8").attr('placeholder', '3－5－40');
						  $("#add9").attr('placeholder', 'スリーシー 101号室');
						  $("#add10").attr('placeholder', '042-111-2222');
						  $("#add11").attr('placeholder', 'kodomohuku@mama.co.jp');
						});
						-->
						</script>

						<html:hidden property="pass" value="1" />
						<html:hidden property="repass" value="1" />
						<html:image page="/buttom/henkou.gif" alt="TAG index" border="0" />
					</html:form>
				</div>
			</div>

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>