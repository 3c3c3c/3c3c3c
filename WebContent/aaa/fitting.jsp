<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html:html>
<head>
<title>Fitting</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="fit.css">

<script type="text/javascript" src="fit.js"></script>
</head>
<body>
	<div id="fitting">
		<img src="s1.png" class="move" name="img1" onMousedown="dragOn('img1')" onmousewheel="zoom('img1')" style="top: 50px; left: 50px; z-index: 2;">
		<img src="84.jpg" class="set">

		<div>
			<b><font size="3">商品画像をマウスでドラッグする事で移動できます。<br>
			 オンマウス状態でスクロールホイールすることで拡大・縮小できます。</font></b>
		</div>
	</div>

	<div align="center">
		<html:img page="/buttom/close_b.gif" onclick="window.close()" style="cursor: pointer" alt="TAG index" border="0" />
	</div>

</body>
</html:html>