<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Go Fit</title>
<LINK rel="stylesheet" type="text/css" href="fit.css">
</head>
<body>
	<div id="fitting">
		<p>お子さんの写真で商品のフィッティングができます。</p>
		<p>写真を選択してください。</p>
		<html:form action="/fittingAction" enctype="multipart-form-data">
			<html:file property="image" accept="jpg|png|bmp|gif" />
				<script type="text/javascript" src="http://www.google.com/jsapi"></script>
				<script type="text/javascript">
					google.load("jquery", "1");
				</script>
				<script type='text/javascript' src='jquery.MultiFile.js'></script>
			<html:image page="/buttom/kettei.gif" />


			<%-- <html:hidden property="productid" value="getImg()" /> --%>
			<html:hidden property="productid" value="
				<script type="text/javascript">
				<!--
					function getImg(){
						str = location.search;
						str = str.substring(1, str.length);
						document.write(str);
					};
				// -->
				</script>
			" />

		</html:form>
		<p>・なるべく全身が映り、正面を向いている画像を選択してください。</p>
		<p>※あくまでイメージです。実際の印象とは異なる場合があります。</p>
	</div>



</body>
</html:html>