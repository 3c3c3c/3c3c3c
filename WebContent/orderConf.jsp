<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@ page import = "java.util.ArrayList,jp.co.ccc.entity.CartEntity" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>カート一覧画面</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div id="contents" align="center">
					<p align="center" style="margin: 20px;">
						<html:img page="/img/process03.png" width="900px" height="120px" />
					</p>

					<div style="margin: 20px;">
						<font><b>注文確認画面</b></font>
					</div>

					<html:form action="/orderConfirmAction" >
						<table border="1">
							<tr align="center" style="background-color: #CCFF99; font-weight: bold;">
								<td>画像</td>
								<td style="width: 200px;">商品名</td>
								<td>サイズ</td>
								<td>単価</td>
								<td>数量</td>
								<td width="130px">合計金額</td>
							</tr>

							<% ArrayList<CartEntity> cart = (ArrayList<CartEntity>)session.getAttribute("cartent"); %>
							<% int total = 0; %>
							<logic:iterate id="data" collection="<%= cart %>" indexId="idx" >
								<% CartEntity cartent = (CartEntity)data; %>
								<tr align="center" style="background-color: white">
									<td>
										<html:img page='<%= "/photo/"+ cartent.getImage() %>' height="100" width="100" /> <br>
									</td>
									<td>
										<%= cartent.getName() %>
									</td>
									<td>
										<%= cartent.getSize() %>
									</td>
									<td>
										\ <%= cartent.getPrice() %> 円(税込)
									</td>
									<td>
										<%= Integer.toString(cartent.getQuantity()) %> 個
									</td>
									<td>
										\ <%= cartent.getPrice() * cartent.getQuantity() %> 円(税込)
										<% total += cartent.getPrice() * cartent.getQuantity(); %>
									</td>
								</tr>
							</logic:iterate>
						<!-- 	<tr align="center" style="background-color: white">
								<td align="center">
									<img src="photo/shirt/shirt1.jpg" height="100" width="100" /></td>
								<td>ハートロゴTシャツ</td>
								<td>80</td>
								<td>￥1,177(税込)</td>
								<td><select name="配送日時" style="font-size: 20px;"disabled>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
								</select>個 </td>
								<td>1,177円</td>
							</tr> -->

							<tr align="right" style="background-color: white">
								<td colspan="6" style="height: 40px;">送料：500円</td>
							</tr>
							<tr align="right" style="background-color: white">
								<td colspan="6" style="height: 40px;">
									<b style="font-size: 24px;">合計：<%= total + 500 %>円</b></td>
							</tr>
						</table>

						<p style="margin: 20px;">
							<font> よろしければ注文確定ボタンを押下してください</font>
						</p>
						<p style="margin: 20px;">
							<html:image page="/buttom/decidech.gif" alt="TAG index" border="0" />
						</p>
					</html:form>
				</div>
			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html>