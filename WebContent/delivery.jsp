<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import="java.util.ArrayList, jp.co.ccc.user.form.UserInfoForm" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Delivery</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<script src="http://www.google.com/jsapi"></script>
	<script>
		google.load("jquery", "1.7");
		google.load("jqueryui", "1.8");
	</script>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<p align="center" style="margin: 20px;">
					<html:img page="/img/process01.png" width="900px" height="120px" />
				</p>

				<div align="center">
					<html:form action="/deliveryAction" >
						<p style="margin-top: 20px"><font>配送日時を指定する</font></p>
						<b>注文日から</b>
						<html:select property="day" value="1" styleId="day1">
							<html:option value="1">指定しない</html:option>
							<html:option value="2">2日後まで</html:option>
							<html:option value="3">3日後まで</html:option>
							<html:option value="4">4日後まで</html:option>
							<html:option value="5">5日後まで</html:option>
							<html:option value="6">6日後まで</html:option>
							<html:option value="7">7日後まで</html:option>
						</html:select>

						<p style="margin-top: 30px"><font>配送時間を指定する</font></p>
						<html:select property="time" value="1" styleId="time1">
							<html:option value="1">指定しない</html:option>
							<html:option value="2">午前中</html:option>
							<html:option value="3">12:00～14:00</html:option>
							<html:option value="4">14:00～16:00</html:option>
							<html:option value="5">16:00～18:00</html:option>
							<html:option value="6">18:00～21:00</html:option>
						</html:select>

						<% UserInfoForm add = (UserInfoForm)session.getAttribute("add"); %>

						<p style="margin-top: 20px"><font><span style="color: red">※</span>指定なしの場合、注文日の2日～5日前後の発送となります</font></p>
						<table border="1">
							<tr>
								<td align="center" style="width: 100px; height: 30px; font-weight: bold; background-color: #CCFF99;">お届け先</td>
								<td align="center" style="width: 100px; height: 30px; font-weight: bold; background-color: #CCFF99;">郵便番号</td>
								<td style="padding-left: 10px; width: 120px; background-color: white;">
									<b><span style="color: red">〒</span><%= add.getPostal() %></b></td>
								<td align="center" style="width: 150px; height: 30px; font-weight: bold; background-color: #CCFF99;">お届け住所</td>
								<td align="center" style="width: 350px; height: 30px; font-weight: bold; background-color: white;">
									<%= add.getAdr_prefectural() + add.getAdr_city() + add.getAdr_town() + add.getAdr_location() %></td>
							</tr>
						</table>

						<html:hidden property="username_family" value='<%= add.getUsername_family() %>' />
						<html:hidden property="username_first" value='<%= add.getUsername_first() %>' />
						<html:hidden property="username_family_ruby" value='<%= add.getUsername_family_ruby() %>' />
						<html:hidden property="username_first_ruby" value='<%= add.getUsername_first_ruby() %>' />
						<html:hidden property="postal" value='<%= add.getPostal() %>' />
						<html:hidden property="adr_prefectural" value='<%= add.getAdr_prefectural() %>' />
						<html:hidden property="adr_city" value='<%= add.getAdr_city() %>' />
						<html:hidden property="adr_town" value='<%= add.getAdr_town() %>' />
						<html:hidden property="adr_location" value='<%= add.getAdr_location() %>' />
						<html:hidden property="tel" value='<%= add.getTel() %>' />
						<html:hidden property="mail" value='<%= add.getMail() %>' />

						<p style="margin: 15px auto;">
							<html:image page="/buttom/sendthis.gif" alt="TAG index" border="0" />
						</p>
						<html:hidden property="pass" value="xxx" />
					</html:form>

					<hr style="margin: 20px;">

					<html:form action="/deliveryAction" >
						<table border="1">
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>姓
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family" style="width: 210px; height: 30px" styleId="add1" />
								</td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>名
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first" style="width: 210px; height: 30px" styleId="add2" />
								</td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>せい</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family_ruby" style="width: 210px; height: 30px" styleId="add3" /></td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>めい</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first_ruby" style="width: 210px; height: 30px" styleId="add4" />
								</td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>郵便番号</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="postal" style="width: 210px; height: 30px" styleId="add5" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>都道府県</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_prefectural"  size="" style="width: 500px; height: 30px" styleId="add6" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>市町村</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_city" style="width: 500px; height: 30px" styleId="add7" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>番地</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_town" style="width: 500px; height: 30px" styleId="add8" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">アパート・マンション名</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="adr_location" style="width: 500px; height: 30px" styleId="add9" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">
									<span style="color: red">※</span>電話番号</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="tel" style="width: 210px; height: 30px" styleId="add10" /></td>
							</tr>
							<tr>
								<td align="center" style="font-weight: bold; background-color: #CCFF99;">メールアドレス</td>
								<td colspan="3" style="padding-left: 5px; background-color: white;">
									<html:text property="mail" style="width: 500px; height: 30px" styleId="add11" /></td>
							</tr>
						</table>
						<p style="margin: 15px auto;"><b style="font-size: 24px;"><span style="color: red">※</span>は必須項目です</b></p>

						<script type="text/javascript">
						<!--
						$(function(){
						  $("#add1").attr('placeholder', '田中');
						  $("#add2").attr('placeholder', '太郎');
						  $("#add3").attr('placeholder', 'たなか');
						  $("#add4").attr('placeholder', 'たろう');
						  $("#add5").attr('placeholder', '012-0123');
						  $("#add6").attr('placeholder', '埼玉県');
						  $("#add7").attr('placeholder', '和光市');
						  $("#add8").attr('placeholder', '3－5－40');
						  $("#add9").attr('placeholder', 'スリーシー 101号室');
						  $("#add10").attr('placeholder', '042-111-2222');
						  $("#add11").attr('placeholder', 'kodomohuku@mama.co.jp');
						});
						-->
						</script>

						<html:hidden property="day" styleId="day2" />
						<html:hidden property="time" styleId="time2" />
						<p style="margin: 15px auto;">
							<html:image page="/buttom/sendthis.gif" alt="TAG index" onclick="getDayTime()" />
						</p>
						<script type="text/javascript">
						  <!--
						  function getDayTime() {
							  document.getElementById("day2").value = document.getElementById("day1").value;
							  document.getElementById("time2").value = document.getElementById("time1").value;
						  }
						  // -->
						</script>
						<% UserInfoForm add = (UserInfoForm)session.getAttribute("add"); %>
						<html:hidden property="pass" value='<%= add.getPass() %>' />
					</html:form>
				</div>
			</div>

			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>
