<%@page import="jp.co.ccc.user.form.UserInfoForm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
 <%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
 <%@page import= "java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% ArrayList<UserInfoForm> list = (ArrayList<UserInfoForm>)request.getAttribute("list");  %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Layout</title>
<LINK rel="stylesheet" type="text/css" href="layout.css">
<LINK rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="container">

		<!----- ヘッダ ----------------------------------------------------------------->
		<div id="boxA"><jsp:include page="/header.jsp" /></div>
		<!----- ヘッダ ----------------------------------------------------------------->

		<div id="wrapper">

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
			<div id="boxB">
				<div align="center">


					<p style="margin: 20px;">
						<font><b> 下記の情報を削除します。<br>
						よろしければ退会を押してください。</b></font>
					</p>

					<html:form action="WithdrawConfirmAction" method="post">
		<% for(int i = 0; i < list.size(); i++){%>
						<table border="1">
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">姓
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family" style="width: 210px; height: 30px" readonly="true" value='<%=list.get(i).getUsername_family() %>' />
								</td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">名
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first" style="width: 210px; height: 30px" readonly="true" value='<%=list.get(i).getUsername_first() %>' />
								</td>
							</tr>
							<tr>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">せい
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_family_ruby" style="width: 210px; height: 30px" readonly="true" value='<%=list.get(i).getUsername_family_ruby() %>'/>
								</td>
								<td align="center" style="width: 200px; font-weight: bold; background-color: #CCFF99;">めい
								</td>
								<td style="padding-left: 5px; background-color: white;">
									<html:text property="username_first_ruby" style="width: 210px; height: 30px" readonly="true" value='<%=list.get(i).getUsername_first_ruby() %>'/>
								</td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">郵便番号</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="postal"
									style="width: 100px; height: 30px" readonly="true" value='<%=list.get(i).getPostal() %>' /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">都道府県</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_prefectural"
									style="width: 570px; height: 30px" readonly="true" value='<%=list.get(i).getAdr_prefectural() %>' /></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">市町村</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_city"
									style="width: 570px; height: 30px" readonly="true" value='<%=list.get(i).getAdr_city() %>'/></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">番地</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_town"
									style="width: 570px; height: 30px" readonly="true" value='<%=list.get(i).getAdr_town() %>'/></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">アパート・マンション名</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="adr_location"
									style="width: 570px; height: 30px" readonly="true" value='<%=list.get(i).getAdr_location() %>'/></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">電話番号</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="tel"
									style="width: 150px; height: 30px" readonly="true" value='<%=list.get(i).getTel() %>'/></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">メールアドレス</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:text property="mail"
									style="width: 570px; height: 30px" readonly="true" value='<%=list.get(i).getMail() %>'/></td>
							</tr>
							<tr>
								<td align="center"
									style="font-weight: bold; background-color: #CCFF99;">パスワード</td>
								<td colspan="3"
									style="padding-left: 5px; background-color: white;"><html:password property="pass"
									style="width: 150px; height: 30px" readonly="true" value='<%=list.get(i).getPass() %>'/></td>
							</tr>
						</table>
			<%} %>
						<p style="margin: 15px;">
							<html:image src="buttom/withdraw_b.gif" alt="TAG index" border="0" />
						</p>
					</html:form>
				</div>
			</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!----- メインコンテンツ ------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


			<!----- 左メニューバー ------------------------------------------------------->
			<div id="boxC"><jsp:include page="/l_side.jsp" /></div>
			<!----- 左メニューバー ------------------------------------------------------->
		</div>

		<!----- 右メニューバー ------------------------------------------------------->
		<div id="boxD"><jsp:include page="/r_side.jsp" /></div>
		<!----- 右メニューバー ------------------------------------------------------->


		<!----- 折り返し解除用フッタ ----->
		<div id="boxE"></div>
		<!----- 折り返し解除用フッタ ----->

	</div>
</body>
</html:html>