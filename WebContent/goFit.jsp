<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Go Fit</title>
<LINK rel="stylesheet" type="text/css" href="fit.css">
</head>
<body>
	<div id="fitting" align="center">
		<p style="margin: 10px; font-size: 20px;">お子さんの写真で商品のフィッティングができます。</p>
		<p>写真を選択してください。</p>

		<html:form action="/fittingAction" enctype="multipart/form-data">
			<script type="text/javascript" src="http://www.google.com/jsapi"></script>
			<script type="text/javascript">
				google.load("jquery", "1");
			</script>
			<script type='text/javascript' src='jquery.MultiFile.js'></script>

			<html:file property="image" styleClass='multi' maxlength='1' accept="jpg|png|bmp|gif" />

			<%-- <html:hidden property="productid" value="getImg()" /> --%>
			<% String id = (String)request.getAttribute("productid");
				if( id == null ){
					id = "100";
				}
			%>
			<html:hidden property="productid" styleId="img" value='<%= id %>' />
				<script type="text/javascript">
				<!--
					str = location.search;
					str = str.substring(1, str.length);
					document.getElementById("img").value = str;
				// -->
				</script>

			<html:image page="/buttom/kettei.gif" style="margin: 10px;" />
		</html:form>
		<p>・なるべく全身が映り、正面を向いている画像を選択してください。</p>
		<p>※あくまでイメージです。実際の印象とは異なる場合があります。</p>
	</div>

</body>
</html:html>