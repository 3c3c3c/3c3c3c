<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<div class="menu">
	<html:form action="/searchKeyWordAction">
		<label style="text-align: center;">
			<html:text style="width: 250px;" property="keyword" />
			<html:image page="/buttom/itemsearch_b.gif" alt="TAG index" border="0" style="vertical-align: bottom;" />
		</label>
	</html:form>
	<html:form action="/searchCategoryAction" >
		<label for="Panel11" style="cursor: pointer; cursor: hand">シャツ</label>
		<input type="checkbox" id="Panel11" class="on-off" />
		<ul>
			<!-- <li><a href="search.jsp">ボーダー柄</a></li> -->
			<li><html:submit property="cate" value="ボーダー柄" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="柄もの" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="その他" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
		</ul>
		<label for="Panel12" style="cursor: pointer; cursor: hand">ズボン</label>
		<input type="checkbox" id="Panel12" class="on-off" />
		<ul>
			<li><html:submit property="cate" value="ハーフパンツ" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="ショートパンツ" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="長ズボン" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
		</ul>
		<label for="Panel13" style="cursor: pointer; cursor: hand">アウター</label>
		<input type="checkbox" id="Panel13" class="on-off" />
		<ul>
			<li><html:submit property="cate" value="コート" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="ジャケット" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="ベスト" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
		</ul>
		<label for="Panel14" style="cursor: pointer; cursor: hand">ぼうし</label>
		<input type="checkbox" id="Panel14" class="on-off" />
		<ul>
			<li><html:submit property="cate" value="キャップ" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="ハット" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="ニット帽" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
		</ul>
		<label for="Panel15" style="cursor: pointer; cursor: hand">くつ</label> <input
			type="checkbox" id="Panel15" class="on-off" />
		<ul>
			<li><html:submit property="cate" value="スニーカー" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="サンダル" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
			<li><html:submit property="cate" value="長ぐつ" style="cursor: pointer;font-size:25px;background-color:#FFE4E1;border-style: none;color:#00008B;"/></li>
		</ul>
	</html:form>
</div>