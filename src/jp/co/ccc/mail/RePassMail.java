package jp.co.ccc.mail;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import jp.co.ccc.dao.DbUtil;

/*
 * パス再発行と購入後の2種類のメールの分岐
 * 分岐する変数と引数
 */

public class RePassMail {
	private static final String ENCODE = "iso-2022-jp";

    public String mail(String mailadr) throws ClassNotFoundException {

    	String temppass = "";

    	try {
    	DbUtil util = new DbUtil();
 		String sql = "select temppass from TEMPPASS_TABLE order by random() limit 1;";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);
		ResultSet rs = pstmt.executeQuery();
			while(rs.next()){
				temppass = rs.getString("temppass");
			}

		util.disStatement(pstmt);
		util.disConnect(util.con);

		} catch (SQLException e) {
			e.printStackTrace();
		}

    	String mail = new RePassMail().txt(temppass);
    	String address = mailadr;
        System.out.println("メール送信開始");

        new RePassMail().process(mail ,address);

        System.out.println("メール送信終了");

        return temppass;
    }

    public void process(String mail , String address) {
        final Properties props = new Properties();

        // 基本情報。ここでは gmailへの接続例を示します。
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        // SSL用にポート番号を変更。
        props.setProperty("mail.smtp.port", "465");

        // タイムアウト設定
        props.setProperty("mail.smtp.connectiontimeout", "60000");
        props.setProperty("mail.smtp.timeout", "60000");

        // 認証
        props.setProperty("mail.smtp.auth", "true");

        // SSL関連設定
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", "465");

        final Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("kaiyokota@gmail.com", "yokotterz2");
            }
        });

        // デバッグを行います。標準出力にトレースが出ます。
        session.setDebug(true);

        // メッセージ内容の設定。
        final MimeMessage message = new MimeMessage(session);
        try {
            final Address addrFrom = new InternetAddress(
                    "kaiyokota@gmail.com", "Mama.com", ENCODE);
            message.setFrom(addrFrom);

            final Address addrTo = new InternetAddress(address,
                    "", ENCODE);
            message.addRecipient(Message.RecipientType.TO, addrTo);

            // メールのSubject
            message.setSubject("Mama.comをご利用いただきありがとうございます!。", ENCODE);

            // メール本文。setTextを用いると 自動的に[text/plain]となる。
            message.setText(mail, ENCODE);

            // 仮対策: 開始
            // setTextを呼び出した後に、ヘッダーを 7bitへと上書きします。
            // これは、一部のケータイメールが quoted-printable を処理できないことへの対策となります。
            message.setHeader("Content-Transfer-Encoding", "7bit");
            // 仮対策: 終了

            // その他の付加情報。
            message.addHeader("X-Mailer", "blancoMail 0.1");
            message.setSentDate(new Date());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // メール送信。
        try {
            Transport.send(message);
        } catch (AuthenticationFailedException e) {
            // 認証失敗は ここに入ります。
            System.out.println("指定のユーザ名・パスワードでの認証に失敗しました。"
                + e.toString());
        } catch (MessagingException e) {
            // smtpサーバへの接続失敗は ここに入ります。
            System.out.println("指定のsmtpサーバへの接続に失敗しました。" + e.toString());
            e.printStackTrace();
        }
    }
    public String txt(String temppass){

    	String user1 = "━━━━━━━━━━━━━━━━━━━━━━━━\n"
    				+ "■本メールはパスワード再発行の自動返信メールとなります\n"
    				+ "━━━━━━━━━━━━━━━━━━━━━━━━\n\n"
    				+ "この度はMama.comをご利用いただき、誠にありがとうございます。\n\n"
    				+ "Mama.comのパスワードの再設定を行います。\n\n"
    				+ "「パスワード再設定」の「本人確認」画面で、次の仮パスワードを入力してください。\n\n\n"
    				+ "仮パスワード        ：";
    	String user2 = temppass ;
    	String user3 = "\n\n\n"
    				+ "※本メールは送信専用です。"
    				+ "もし何か不具合やご不明な点がございましたら、いつでも"
    				+ "ご遠慮なくお申しつけくださいませ。\n\n"
    				+ "今後ともMama.comをよろしくお願いいたします。\n\n"
    				+ "-------------------\n"
    				+ "株式会社3C(スリーシー)\n"
    				+ "横田 海\n"
    				+ "〒123-4567 東京都豊島区南大塚1-1-1\n"
    				+ "TEL：03-1111-2222\n"
    				+ "mail：14o_yokota@axiz.co.jp\n"
    				;
    	String user = user1 + user2 + user3;
		return user;
    }
}