package jp.co.ccc.mail;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.entity.CartEntity;

/*
 * パス再発行と購入後の2種類のメールの分岐
 * 分岐する変数と引数
 */

public class Mail {
	private static final String ENCODE = "iso-2022-jp";

    public void mail(int orderid , ArrayList<CartEntity> cart) throws ClassNotFoundException {

    	ArrayList<Object> detail = new ArrayList<Object>();
    	ArrayList<Integer> total_detailList = new ArrayList<Integer>();
    	ArrayList<Integer> product = new ArrayList<Integer>();
    	ArrayList<String> product_name = new ArrayList<String>();
    	ArrayList<String> price_name = new ArrayList<String>();
    	int orderId = orderid;
    	String sql = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	CartEntity cartent = null;;
    	int userid = 0;
    	String deliveryday = "";
    	String username = "";
    	String username_first = "";
    	String username_family = "";
    	String str_total = "";
    	String str_totalAmount = "";
    	String address = "";
    	String str_totalPrice = "";
    	int productid = 0;
    	int sizeid =0;
    	int quantity = 0;
    	int totalAmount = 0;//・・・・・・・・・・・合計金額（商品合計金額 + 送料）
    	int totalPrice = 0;//・・・・・・・・・・・商品合計金額
    	int delCharges = 500;//・・・・・・・・・・・送料
    	try {
        	DbUtil util = new DbUtil();





//ORDER_TABLEからユーザIDと配達日を抽出
     		sql = "select userid,deliveryday from ORDER_TABLE where orderid = "+orderId+";";
    		pstmt = util.stateConnect(util.connect(),sql);
    		rs = pstmt.executeQuery();
    			while(rs.next()){
    				userid= rs.getInt("userid");
    				deliveryday= rs.getString("deliveryday");
    			}
    		util.disStatement(pstmt);





//USER_TABLEから名前を抽出
    		sql = "select username_family,username_first,mail from USER_TABLE where userid = "+userid+";";
    		pstmt = util.stateConnect(util.connect(),sql);
    		rs = pstmt.executeQuery();
    			while(rs.next()){
    				username_family = rs.getString("username_family");
    				username_first= rs.getString("username_first");
    				address = rs.getString("mail");
    			}
    			username = username_family + username_first;
    		util.disStatement(pstmt);





//ORDERDETAIL_TALEから商品IDを抽出
    		sql = "select productid,sizeid,quantity from ORDERDETAIL_TABLE where orderid = "+orderId+";";
    		pstmt = util.stateConnect(util.connect(),sql);
    		rs = pstmt.executeQuery();
    			while(rs.next()){
    				product.add(rs.getInt("productid"));
    			}
    		util.disStatement(pstmt);





//PRODUCT_TABLEから商品名を抽出
    		for (Iterator it = product.iterator(); it.hasNext();) {
    			sql = "select productname,price from PRODUCT_TABLE where productid = "+it.next()+";";
        		pstmt = util.stateConnect(util.connect(),sql);
        		rs = pstmt.executeQuery();
        			while(rs.next()){
        				product_name.add(rs.getString("productname"));
        				price_name.add(Integer.toString(rs.getInt("price")));
        			}
        		util.disStatement(pstmt);
    		}





//
    		sql = "select sizeid,quantity from ORDERDETAIL_TABLE where orderid = "+orderId+";";
    		pstmt = util.stateConnect(util.connect(),sql);
    		int i = 0;
    		rs = pstmt.executeQuery();

    			while(rs.next()){
    				detail.add(product_name.get(i));
    				detail.add(rs.getInt("quantity"));
    				detail.add(price_name.get(i));
    				totalPrice += ((rs.getInt("quantity"))*(Integer.parseInt(price_name.get(i))));
    				i++;
    			}
    		util.disStatement(pstmt);





    		str_totalPrice = Integer.toString(totalPrice);
    		str_totalAmount = Integer.toString(totalPrice + 500);
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}





    	String mail = new Mail().txt(deliveryday,username,
    			 detail, str_totalPrice,str_totalAmount
    			);

        System.out.println("メール送信開始");

        new Mail().process(mail,address);

        System.out.println("メール送信終了");
    }

    public void process(String mail,String address) {
        final Properties props = new Properties();

        // 基本情報。ここでは gmailへの接続例を示します。
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        // SSL用にポート番号を変更。
        props.setProperty("mail.smtp.port", "465");

        // タイムアウト設定
        props.setProperty("mail.smtp.connectiontimeout", "60000");
        props.setProperty("mail.smtp.timeout", "60000");

        // 認証
        props.setProperty("mail.smtp.auth", "true");

        // SSL関連設定
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", "465");

        final Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("kaiyokota@gmail.com", "yokotterz2");
            }
        });

        // デバッグを行います。標準出力にトレースが出ます。
        session.setDebug(true);

        // メッセージ内容の設定。
        final MimeMessage message = new MimeMessage(session);
        try {
            final Address addrFrom = new InternetAddress(
                    "kaiyokota@gmail.com", "Mama.com", ENCODE);
            message.setFrom(addrFrom);

            final Address addrTo = new InternetAddress(address,
                    "", ENCODE);
            message.addRecipient(Message.RecipientType.TO, addrTo);

            // メールのSubject
            message.setSubject("Mama.comをご利用いただきありがとうございます。", ENCODE);

            // メール本文。setTextを用いると 自動的に[text/plain]となる。
            message.setText(mail, ENCODE);

            // 仮対策: 開始
            // setTextを呼び出した後に、ヘッダーを 7bitへと上書きします。
            // これは、一部のケータイメールが quoted-printable を処理できないことへの対策となります。
            message.setHeader("Content-Transfer-Encoding", "7bit");
            // 仮対策: 終了

            // その他の付加情報。
            message.addHeader("X-Mailer", "blancoMail 0.1");
            message.setSentDate(new Date());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // メール送信。
        try {
            Transport.send(message);
        } catch (AuthenticationFailedException e) {
            // 認証失敗は ここに入ります。
            System.out.println("指定のユーザ名・パスワードでの認証に失敗しました。"
                + e.toString());
        } catch (MessagingException e) {
            // smtpサーバへの接続失敗は ここに入ります。
            System.out.println("指定のsmtpサーバへの接続に失敗しました。" + e.toString());
            e.printStackTrace();
        }
    }
    public String txt(String deliveryday,String username,ArrayList<Object> detail,String  str_totalPrice,String str_totalAmount){

    	String a = "";
    	String b = "";
    	String c = "";
    	String user2 = "";

    	String user1 = "━━━━━━━━━━━━━━━━━━━━━━━━\n"
    				+ "■本メールは注文受付の自動返信メールとなります\n"
    				+ "━━━━━━━━━━━━━━━━━━━━━━━━\n\n"
    				+  username+"様\n\n"
    				+ "この度はMama.comをご利用いただき、誠にありがとうございます。\n\n"
    				+ "確かに下記内容のご注文の入力を承りましたので、"
    				+ "注意事項とご注文内容をご確認下さいませ。\n"
    				+ "【注文商品】\n\n";

    				for (Iterator it = detail.iterator(); it.hasNext();) {
    				 a = "商品名        ："+it.next()+"\n";
    				 b = "数量          ："+it.next()+"\n";
    				 c = "単価(税込)    ："+it.next()+"円\n\n";
    				 user2 += a+b+c;
    				}

    	String user3 =
    				 "-------------------------------------------------------------\n"
    				+ "商品合計      ："+str_totalPrice+"円\n"
    				+ "送料          ："+500+"円\n"
    				+ "合計金額      ："+str_totalAmount+"円\n\n"
    				+ "【発送予定日】\n\n"
    				+ deliveryday+"\n\n"
    				+ "≪注意事項≫\n\n"
    				+ "【商品の配送時期について】\n\n"
    				+ "1.特に記載がない場合            通常5営業日以内に発送いたします。\n"
    				+ "2.商品説明に明記がある場合      商品説明の記載に従います。\n"
    				+ "上記の通り注文を承りました。\n"
    				+ "もし何か不具合やご不明な点がございましたら、いつでも"
    				+ "ご遠慮なくお申しつけくださいませ。\n\n"
    				+ "今後ともMama.comをよろしくお願いいたします。\n\n"
    				+ "-------------------\n"
    				+ "株式会社3C(スリーシー)\n"
    				+ "横田 海\n"
    				+ "〒123-4567 東京都豊島区南大塚1-1-1\n"
    				+ "TEL：03-1111-2222\n"
    				+ "mail：14o_yokota@axiz.co.jp\n"
    				;
		String user = user1 + user2 + user3;
    	return user;
    }
}