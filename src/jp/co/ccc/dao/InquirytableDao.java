package jp.co.ccc.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.ccc.entity.InquirytableEntity;

public class InquirytableDao {

	//InquirytableEntityにセットされた値をもとに検索を行い、結果を返す。
	public ArrayList<InquirytableEntity> inquiryselect(InquirytableEntity inqeiryEnt) throws SQLException{

		return null;
	}

	//InquirytableEntityにセットされた値を用いて、DBに値を登録する。
	public int inquiryInsert(InquirytableEntity inqeiryEnt) throws SQLException{

		return 0;
	}

	//InquirytableEntityの更新内容を指定し、DB内のデータを更新する。
	public int inquiryUpdate(InquirytableEntity inqeiryEnt) throws SQLException{

		return 0;
	}

	//InquirytableEntityにセットされた情報をキーに、DB内からデータを削除する。
	public int inquiryDelete(InquirytableEntity inqeiryEnt) throws SQLException{

		return 0;
	}
}