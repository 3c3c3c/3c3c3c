package jp.co.ccc.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.ccc.user.form.ProductForm;

public class ProducttableDao {

	//ProductFormにセットされた値をもとに検索を行い、結果を返す。
	public ArrayList<ProductForm> productSelect(ProductForm productinfo) throws SQLException{

		return null;
	}

	//ProductEntryFormにセットされた値を用いて、DBに値を登録する。
	public int productInsert(ProductForm productinfo) throws SQLException{

		return 0;
	}

	//ProductFormの更新内容を指定し、DB内のデータを更新する。
	public int productUpdate(ProductForm productinfo) throws SQLException{

		return 0;
	}

	//ProductFormにセットされた情報をキーに、DB内からデータを削除する。
	public int productDelete(ProductForm productinfo) throws SQLException{

		return 0;
	}
}