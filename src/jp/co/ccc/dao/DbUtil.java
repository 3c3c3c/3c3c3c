package jp.co.ccc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DbUtil {

	public static Connection con;
	public static PreparedStatement stmt;

	public static Connection connect() throws SQLException , ClassNotFoundException{

		Class.forName("org.postgresql.Driver");
		Connection con = DriverManager.getConnection("jdbc:postgresql:mamadb","axizuser","axiz");
		return con;
	}

	public static PreparedStatement stateConnect(Connection con , String sql) throws SQLException{
		PreparedStatement stmt = con.prepareStatement(sql);
		return stmt;
	}

	public static void disStatement(PreparedStatement stmt) throws SQLException{
		if(stmt != null) {
			stmt.close();
			stmt = null;
		}
	}

	public static void disConnect(Connection con) throws SQLException{
		if(con != null) {
			con.close();
			con = null;
		}
	}

}
