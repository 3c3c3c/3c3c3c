package jp.co.ccc.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.ccc.user.form.UserInfoForm;

public class UsertableDao {

	//UserInfoFormにセットされた値をもとに検索を行い、結果を返す。
	public ArrayList<UserInfoForm> userSelect(UserInfoForm userinfo) throws SQLException, ClassNotFoundException{

		return null;
	}

	//UserInfoFormにセットされた値を用いて、DBに値を登録する。
	public int userInsert(UserInfoForm userinfo) throws SQLException{

		return 0;
	}

	//UserInfoFormの更新内容を指定し、DB内のデータを更新する。
	public int userUpdate(UserInfoForm userinfo) throws SQLException{

		return 0;
	}

	//UserInfoFormにセットされた情報をキーに、DB内からデータを削除する。
	public int userDelete(UserInfoForm userinfo) throws SQLException{

		return 0;
	}
}
