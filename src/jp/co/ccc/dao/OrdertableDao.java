package jp.co.ccc.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.ccc.entity.OrdertableEntity;

public class OrdertableDao {

	//OrdertableEntityにセットされた値をもとに検索を行い、結果を返す。
	public ArrayList<OrdertableEntity> orderSelect(OrdertableEntity orderEnt) throws SQLException{

		return null;
	}

	//OrdertableEntityにセットされた値を用いて、DBに値を登録する。
	public int orderInsert(OrdertableEntity orderEnt) throws SQLException{

		return 0;
	}

	//OrdertableEntityの更新内容を指定し、DB内のデータを更新する。
	public int orderUpdate(OrdertableEntity orderEnt) throws SQLException{

		return 0;
	}

	//OrdertableEntityにセットされた情報をキーに、DB内からデータを削除する。
	public int orderDelete(OrdertableEntity orderEnt) throws SQLException{

		return 0;
	}
}
