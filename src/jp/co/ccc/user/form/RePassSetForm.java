package jp.co.ccc.user.form;

import org.apache.struts.validator.ValidatorForm;

public class RePassSetForm extends ValidatorForm{
	private String pass;
	private String repass;
	private String mail;

	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getRepass() {
		return repass;
	}
	public void setRepass(String repass) {
		this.repass = repass;
	}

}
