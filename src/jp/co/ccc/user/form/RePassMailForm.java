package jp.co.ccc.user.form;

import org.apache.struts.validator.ValidatorForm;

public class RePassMailForm extends ValidatorForm{
	private String mail;
	private String username_family;
	private String username_first;

	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getUsername_family() {
		return username_family;
	}
	public void setUsername_family(String username_family) {
		this.username_family = username_family;
	}
	public String getUsername_first() {
		return username_first;
	}
	public void setUsername_first(String username_first) {
		this.username_first = username_first;
	}




}
