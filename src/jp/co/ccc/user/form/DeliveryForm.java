package jp.co.ccc.user.form;

/*import org.apache.struts.validator.ValidatorForm;


public class DeliveryForm extends ValidatorForm{*/
public class DeliveryForm extends UserInfoForm{
	private String day;
	private String time;

	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}


}
