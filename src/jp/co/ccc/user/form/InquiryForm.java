package jp.co.ccc.user.form;

import org.apache.struts.validator.ValidatorForm;

public class InquiryForm extends ValidatorForm{
	private String[] reply = new String[20];

    public void setReply(String[] reply) {
        this.reply = reply;
    }

    public String[] getReply() {
        return reply;
    }

}
