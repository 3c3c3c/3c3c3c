package jp.co.ccc.user.form;

import org.apache.struts.validator.ValidatorForm;

public class TempPassForm  extends ValidatorForm{

	private String temppass;

	public String getTemppass() {
		return temppass;
	}

	public void setTemppass(String temppass) {
		this.temppass = temppass;
	}
}
