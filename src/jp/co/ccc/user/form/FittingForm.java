package jp.co.ccc.user.form;

//import org.apache.struts.upload.FormFile;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

public class FittingForm extends ValidatorForm {
	private FormFile image;
	private String productid;

	public FormFile getImage() {
		return image;
	}
	public void setImage(FormFile image) {
		this.image = image;
	}
	public String getProductid() {
		return productid;
	}
	public void setProductid(String productid) {
		this.productid = productid;
	}
}
