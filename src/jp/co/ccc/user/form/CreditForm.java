package jp.co.ccc.user.form;

import org.apache.struts.validator.ValidatorForm;

public class CreditForm extends ValidatorForm{
	private String cardoffice;
	private String cardnum;
	private String cardlimit;
	private String cardname;
	private String cardsecurity;
	private String payway = "credit";

	public String getCardoffice() {
		return cardoffice;
	}
	public void setCardoffice(String cardoffice) {
		this.cardoffice = cardoffice;
	}
	public String getCardnum() {
		return cardnum;
	}
	public void setCardnum(String cardnum) {
		this.cardnum = cardnum;
	}
	public String getCardlimit() {
		return cardlimit;
	}
	public void setCardlimit(String cardlimit) {
		this.cardlimit = cardlimit;
	}
	public String getCardname() {
		return cardname;
	}
	public void setCardname(String cardname) {
		this.cardname = cardname;
	}
	public String getCardsecurity() {
		return cardsecurity;
	}
	public void setCardsecurity(String cardsecurity) {
		this.cardsecurity = cardsecurity;
	}
	public String getPayway() {
		return payway;
	}
	public void setPayway(String payway) {
		this.payway = payway;
	}

}
