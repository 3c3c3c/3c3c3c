package jp.co.ccc.user.form;

import org.apache.struts.validator.ValidatorForm;

public class WithDrawForm extends ValidatorForm{
	private String mail;
	private String pass;
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}



}
