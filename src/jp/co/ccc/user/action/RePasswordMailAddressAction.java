package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.RePasswordForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RePasswordMailAddressAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		/*
		 * 入力されたメールアドレスがユーザのものか確認する
		 * 確認した後に再発行手続きへ進む
		 */
		RePasswordForm repassmail = (RePasswordForm)form;
		String mail = repassmail.getMail();
		Boolean flg = true;

		DbUtil util = new DbUtil();

		String sql = "select mail from USER_TABLE where mail = '"+mail+"'";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);
		ResultSet rs = pstmt.executeQuery();

		while(rs.next()){
			flg = false;
		}

		util.disStatement(pstmt);
		util.disConnect(util.con);

		if(flg){
			return (mapping.findForward("failed"));
		}else{
			request.setAttribute("mail",mail);
			return (mapping.findForward("success"));
		}
	}
}
