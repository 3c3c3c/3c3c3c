package jp.co.ccc.user.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.entity.InquirytableEntity;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InquiryAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ArrayList<InquirytableEntity> inquiry = new ArrayList<InquirytableEntity>();

		Connection con = null;
		PreparedStatement stmt = null;
		String sql = "";
		try{
			con = DbUtil.connect();
			sql = "SELECT * FROM INQUIRY_TABLE;";
			stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()){
				InquirytableEntity inquiryent = new InquirytableEntity();
				inquiryent.setInquiry(rs.getString("inquiry"));
				inquiryent.setChoice1(rs.getString("choice1"));
				inquiryent.setChoice2(rs.getString("choice2"));
				inquiryent.setChoice3(rs.getString("choice3"));
				inquiryent.setChoice4(rs.getString("choice4"));
				inquiryent.setChoice5(rs.getString("choice5"));
				inquiry.add(inquiryent);
			}
			request.setAttribute("inquiry", inquiry);

		}
		catch(Exception e){
			e.printStackTrace();
			return (mapping.findForward("failed"));
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}

		return (mapping.findForward("success"));
	}
}
