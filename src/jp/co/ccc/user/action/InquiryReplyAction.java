package jp.co.ccc.user.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.InquiryForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InquiryReplyAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InquiryForm inquiryForm = (InquiryForm)form;

		String reply[] = inquiryForm.getReply();
		ArrayList<Integer> inquiryid = new ArrayList<Integer>();

		Connection con = null;
		PreparedStatement stmt = null;
		String sql = "";
		try{
			con = DbUtil.connect();
			sql = "SELECT inquiryid FROM INQUIRYRESULT_TABLE;";
			stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				inquiryid.add(rs.getInt("inquiryid"));
			}
			DbUtil.disStatement(stmt);

			String column = "result";
			for(int i=0 ; i<reply.length ; i++){
				if( reply[i] != null ){
					sql = "UPDATE INQUIRYRESULT_TABLE SET "+ column + reply[i] +" = "+ column + reply[i] +" + 1,"+
								" sum = sum + 1 WHERE inquiryid = ?;";
					stmt = con.prepareStatement(sql);
					stmt.setInt(1,  inquiryid.get(i));
					stmt.executeUpdate();
					DbUtil.disStatement(stmt);
				}
			}

		}
		catch(Exception e){
			e.printStackTrace();
			return (mapping.findForward("failed"));
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}

		return (mapping.findForward("success"));
	}
}
