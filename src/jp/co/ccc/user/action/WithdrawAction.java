package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.UserInfoForm;
import jp.co.ccc.user.form.WithDrawForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class WithdrawAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		WithDrawForm withDrawForm = (WithDrawForm) form;



		String mail = withDrawForm.getMail();
		String pass = withDrawForm.getPass();

		//セッションを取得
		HttpSession session = request.getSession();

		UserInfoForm user = new UserInfoForm();
		user = (UserInfoForm)session.getAttribute("login");
		int idnum = user.getUserid();

		//ログイン者情報の取得
		String sql= "SELECT * FROM USER_TABLE WHERE userid = " + idnum + "";

		PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
		ResultSet rs = stmt.executeQuery();

		ArrayList<UserInfoForm> list = new ArrayList<UserInfoForm>();
		String dbmail = "";
		String dbpass = "";

		while(rs.next()){
			UserInfoForm uif = new UserInfoForm();
			uif.setUsername_family(rs.getString("username_family"));
			uif.setUsername_first(rs.getString("username_first"));
			uif.setUsername_family_ruby(rs.getString("username_family_ruby"));
			uif.setUsername_first_ruby(rs.getString("username_first_ruby"));
			uif.setPostal(rs.getString("postal"));
			uif.setAdr_prefectural(rs.getString("adr_prefectural"));
			uif.setAdr_city(rs.getString("adr_city"));
			uif.setAdr_town(rs.getString("adr_town"));
			uif.setAdr_location(rs.getString("adr_location"));
			uif.setTel(rs.getString("tel"));
			uif.setMail(rs.getString("mail"));
			uif.setPass(rs.getString("pass"));

			dbmail = rs.getString("mail");
			dbpass = rs.getString("pass");

			list.add(uif);
		}

		DbUtil.disStatement(stmt);

		//入力値の一致判定
		if(dbmail.equals(mail) && dbpass.equals(pass)){
			request.setAttribute("list", list);
		}else{
			request.setAttribute("wderror", "メールアドレスまたはPASSが正しくありません");

			return (mapping.findForward("failed"));
		}

		return (mapping.findForward("success"));
	}
}
