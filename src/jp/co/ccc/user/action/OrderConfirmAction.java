package jp.co.ccc.user.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.entity.CartEntity;
import jp.co.ccc.mail.Mail;
import jp.co.ccc.user.form.CreditForm;
import jp.co.ccc.user.form.DeliveryForm;
import jp.co.ccc.user.form.UserInfoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class OrderConfirmAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserInfoForm login = (UserInfoForm)session.getAttribute("login");
		ArrayList<CartEntity> cart = (ArrayList<CartEntity>)session.getAttribute("cartent");
		DeliveryForm deli = (DeliveryForm)session.getAttribute("deli");
		CreditForm pay = (CreditForm)session.getAttribute("pay");

		Connection con = null;
		PreparedStatement stmt = null;
		String sql = "";
		try{
			con = DbUtil.connect();
			//ORDER_TABLE�ɓo�^
			Calendar cal = Calendar.getInstance();
			String date = "";
			date += Integer.toString( cal.get(Calendar.YEAR) ) +"/";
			date += Integer.toString( cal.get(Calendar.MONTH)+1 ) +"/";
			date += Integer.toString( cal.get(Calendar.DATE) );

			if( deli.getDay() == "1" ){
				cal.add(Calendar.DAY_OF_YEAR,  (int)(Math.random() * 7) + 1);
			}
			else{
				cal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(deli.getDay()));
			}
			String del = "";
			del += Integer.toString( cal.get(Calendar.YEAR) ) +"/";
			del += Integer.toString( cal.get(Calendar.MONTH)+1 ) +"/";
			del += Integer.toString( cal.get(Calendar.DATE) );

			sql = "INSERT INTO ORDER_TABLE(userid, orderday, deliveryday, deliverytime) VALUES(?, ?, ?, ?)";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, login.getUserid());
			stmt.setString(2, date);
			stmt.setString(3, del);
			stmt.setString(4, deli.getTime());
			stmt.executeUpdate();
			DbUtil.disStatement(stmt);


			//ORDERDETAIL_TABLE�ɓo�^
			sql = "SELECT orderid FROM ORDER_TABLE WHERE userid= ? AND orderday = ? AND deliveryday = ? AND deliverytime = ?;";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, login.getUserid());
			stmt.setString(2, date);
			stmt.setString(3, del);
			stmt.setString(4, deli.getTime());
			ResultSet rs = stmt.executeQuery();
			int orderid = 0;
			while( rs.next() ){
				orderid = rs.getInt("orderid");
			}
			DbUtil.disStatement(stmt);

			if( orderid != 0 ){
				for( CartEntity cartent : cart ){
					sql = "INSERT INTO ORDERDETAIL_TABLE VALUES(?, ?, ?, ?);";
					stmt = con.prepareStatement(sql);
					stmt.setInt(1, orderid);
					stmt.setInt(2, cartent.getId());
					stmt.setInt(3, cartent.getSize());
					stmt.setInt(4, cartent.getQuantity());
					stmt.executeUpdate();
					DbUtil.disStatement(stmt);
				}
				Mail sendMail = new Mail();
				sendMail.mail(orderid,cart);
			}
			else{
				return (mapping.findForward("failed"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return (mapping.findForward("failed"));
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}

		session.setAttribute("cartent", null);
		session.setAttribute("deli", null);
		session.setAttribute("pay", null);

		return (mapping.findForward("success"));
	}
}
