package jp.co.ccc.user.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.entity.CartEntity;
import jp.co.ccc.user.form.ProductForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ListupAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentType�̐ݒ�
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		ArrayList<CartEntity> cartent = (ArrayList<CartEntity>)session.getAttribute("cartent");
		if( cartent == null ){
			cartent = new ArrayList<CartEntity>();
			session.setAttribute("cartent", cartent);
		}

		ProductForm productForm = (ProductForm)form;
		int productid = productForm.getProductid();

		Connection con = null;
		PreparedStatement stmt = null;
		String sql = "";
		try{
			con = DbUtil.connect();
			sql = "SELECT * FROM PRODUCT_TABLE WHERE productid = ?;";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, productid);
			ResultSet rs = stmt.executeQuery();

			rs.next();

			CartEntity ce = new CartEntity();
			ce.setId(rs.getInt("productid"));
			ce.setName(rs.getString("productname"));
			//ce.setSize(productForm.getSize());
			ce.setPrice(rs.getInt("price"));
			ce.setQuantity(productForm.getNum());
			ce.setImage(rs.getString("productimage"));
			DbUtil.disStatement(stmt);

			sql = "SELECT size FROM SIZE_TABLE WHERE sizeid = ?;";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, productForm.getSize());
			rs = stmt.executeQuery();

			rs.next();

			ce.setSize(rs.getInt("size"));

			cartent.add(ce);
		}catch(Exception e){
			e.printStackTrace();
			return (mapping.findForward("failed"));
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}
		return (mapping.findForward("success"));
	}
}
