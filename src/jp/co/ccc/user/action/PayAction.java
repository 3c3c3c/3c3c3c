package jp.co.ccc.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.user.form.CreditForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PayAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		CreditForm creditForm = (CreditForm)form;

		HttpSession session = request.getSession();

		if( creditForm.getPayway().equals("credit") ){
			CreditForm pay = new CreditForm();

			pay.setCardoffice(creditForm.getCardoffice());
			pay.setCardnum(creditForm.getCardnum());
			pay.setCardlimit(creditForm.getCardlimit());
			pay.setCardname(creditForm.getCardname());
			pay.setCardsecurity(creditForm.getCardsecurity());

			session.setAttribute("pay", pay);
		}

		return (mapping.findForward("success"));
	}
}
