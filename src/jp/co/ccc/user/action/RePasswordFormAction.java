package jp.co.ccc.user.action;

import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.RePassSetForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RePasswordFormAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		/*
		 * パスワードを再発行するクラス
		 * 最初の入力と2回目の入力が正しければ再発行する
		 * 間違っていれば再発行画面へ戻る
		 */


		RePassSetForm repassset = (RePassSetForm)form;
		String pass = repassset.getPass();
		String repass = repassset.getRepass();
		String mail = repassset.getMail();
		if(pass.equals(repass)){

		DbUtil util = new DbUtil();

		String sql = "update USER_TABLE set pass = '"+pass+"' where mail = '"+mail+"'";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);
		int rs = pstmt.executeUpdate();

		util.disStatement(pstmt);
		util.disConnect(util.con);

		return (mapping.findForward("success"));

		}else{
			return (mapping.findForward("failed"));
		}
	}
}
