package jp.co.ccc.user.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.FittingForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class FittingAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		FittingForm fittingForm = (FittingForm)form;

		String productid = fittingForm.getProductid();

		Connection con = null;
		PreparedStatement stmt = null;
		String sql = "";
		try{
			con = DbUtil.connect();
			sql = "SELECT productimage FROM PRODUCT_TABLE WHERE productid = ?;";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, Integer.parseInt(productid));
			ResultSet rs = stmt.executeQuery();

			while(rs.next()){
				request.setAttribute("fitimg", rs.getString("productimage"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
			request.setAttribute("productid", productid);
			return (mapping.findForward("failed"));
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}

		FormFile image = fittingForm.getImage();

		if( image.getFileName().equals("") ){
			request.setAttribute("productid", productid);
			return (mapping.findForward("failed"));
		}

		ServletContext sc = getServlet().getServletContext();

		InputStream is = image.getInputStream();

		BufferedInputStream inBuffer = new BufferedInputStream(is);

		FileOutputStream fos = new FileOutputStream
				(sc.getRealPath("/upload")+ "/"
						+ image.getFileName());

		BufferedOutputStream outBuffer = new BufferedOutputStream(fos);

		int contents = 0;

		while ((contents = inBuffer.read()) != -1) {
			outBuffer.write(contents);
		}

		outBuffer.flush();
		inBuffer.close();
		outBuffer.close();

		request.setAttribute("image", image.getFileName());

		image.destroy();

		return (mapping.findForward("success"));
	}
}
