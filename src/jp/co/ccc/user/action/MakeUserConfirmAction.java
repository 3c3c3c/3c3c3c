package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.admin.form.UserForm;
import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.UserInfoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MakeUserConfirmAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserInfoForm userInfoForm = (UserInfoForm)form;

		//MakeUserAction�ł�ArrayList���g�p���Ă��邯�ǁA�������͎g���ĂȂ�
		//DB�ɂȂ���Ȃ�

		//�e���͒l���󂯎��

		String name1 = userInfoForm.getUsername_family();
		String name2 = userInfoForm.getUsername_first();
		String name3 = userInfoForm.getUsername_family_ruby();
		String name4 = userInfoForm.getUsername_first_ruby();
		String post = userInfoForm.getPostal();
		String adr1 = userInfoForm.getAdr_prefectural();
		String adr2 = userInfoForm.getAdr_city();
		String adr3 = userInfoForm.getAdr_town();
		String adr4 = userInfoForm.getAdr_location();
		String tel = userInfoForm.getTel();
		String ml = userInfoForm.getMail();
		String pass = userInfoForm.getPass();
		String repass = userInfoForm.getRepass();



		System.out.println(name1);
		System.out.println(name2);
		System.out.println(name3);
		System.out.println(name4);
		System.out.println(post);
		System.out.println(adr1);
		System.out.println(adr2);
		System.out.println(adr3);
		System.out.println(adr4);
		System.out.println(tel);
		System.out.println(ml);
		System.out.println(pass);
		System.out.println(repass);

		//�e�ϐ���������
		boolean fig = true;


		//����PASS�ƍē���PASS����v���邩�m�F
		if(!pass.equals(repass)){

			request.setAttribute("username_family",name1);
			request.setAttribute("username_first",name2);
			request.setAttribute("username_family_ruby",name3);
			request.setAttribute("username_first_ruby",name4);
			request.setAttribute("postal",post);
			request.setAttribute("adr_prefectural",adr1);
			request.setAttribute("adr_city",adr2);
			request.setAttribute("adr_town",adr3);
			request.setAttribute("adr_location",adr4);
			request.setAttribute("tel",tel);
			request.setAttribute("mail",ml);
			request.setAttribute("pass",pass);
			request.setAttribute("repass",repass);
		}

		//�ē��͂���PASS����v�����ꍇ�f�[�^�x�[�X�ɓo�^
		if(fig){
			try{
				DbUtil util = new DbUtil();

				int id = 10;

				//�V�KID���l��
				ArrayList<UserForm> list = new ArrayList<UserForm>();
				String selsql = "SELECT * FROM USER_TABLE;";
				PreparedStatement selpstmt = util.stateConnect(util.connect(), selsql);
				ResultSet exist = selpstmt.executeQuery();
				while(exist.next()){
					UserForm expef = new UserForm();
					expef.setUserid(exist.getInt("userid"));
					list.add(expef);
				}

				//������ID���i�[����R���N�V������p��
				ArrayList<Integer> check = new ArrayList<Integer>();
				for(int i = 0; i < list.size(); i++){
					check.add(list.get(i).getUserid());
				}
				//ID�̎����̔�
				int idnum = 0;
				for(idnum = 1001; ;idnum++){
					int count = 0;
					for(int j = 0; j< check.size(); j++){
						if(check.get(j) == idnum){
							count ++;
						}
					}
					if(count == 0){
						break;
					}
				}

				//�o�^�����{��
				String sql = "INSERT INTO USER_TABLE"
						+ "(userid,"
						+ " username_family,"
						+ " username_first,"
						+ " username_family_ruby,"
						+ " username_first_ruby,"
						+ " postal,"
						+ " adr_prefectural,"
						+ " adr_city,"
						+ " adr_town,"
						+ " adr_location,"
						+ " tel,"
						+ " mail,"
						+ " pass"
						+ ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);";
				PreparedStatement stmt1 = util.stateConnect(util.connect(), sql);
				stmt1.setInt(1, idnum);
				stmt1.setString(2, name1);
				stmt1.setString(3, name2);
				stmt1.setString(4, name3);
				stmt1.setString(5, name4);
				stmt1.setString(6, post);
				stmt1.setString(7, adr1);
				stmt1.setString(8, adr2);
				stmt1.setString(9, adr3);
				stmt1.setString(10, adr4);
				stmt1.setString(11, tel);
				stmt1.setString(12,ml);
				stmt1.setString(13, pass);

				int rs = stmt1.executeUpdate();


				//�f�[�^�x�[�X�A�X�e�[�g�����g�ؒf
				util.disStatement(stmt1);
				util.disConnect(util.con);

				request.setAttribute("username_family",name1);
				request.setAttribute("username_first",name2);
				request.setAttribute("username_family_ruby",name3);
				request.setAttribute("username_first_ruby",name4);
				request.setAttribute("postal",post);
				request.setAttribute("adr_prefectural",adr1);
				request.setAttribute("adr_city",adr2);
				request.setAttribute("adr_town",adr3);
				request.setAttribute("adr_location",adr4);
				request.setAttribute("tel",tel);
				request.setAttribute("mail",ml);
				request.setAttribute("pass",pass);
				request.setAttribute("repass",repass);

				//���O�C����Ԃ̃Z�b�V��������
				HttpSession session = request.getSession();

				UserInfoForm user = new UserInfoForm();
				user.setUserid(idnum);
				user.setUsername_family(name1);
				user.setUsername_first(name2);
				session.setAttribute("login", user);

			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return (mapping.findForward("success"));
	}
}
