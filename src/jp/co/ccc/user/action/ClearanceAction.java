package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.ProductForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ClearanceAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentTypeの設定
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");
		try{
			//データベース接続
			DbUtil util = new DbUtil();

			String sql = "SELECT * FROM PRODUCT_TABLE ORDER BY stock ASC limit 6;";
			PreparedStatement stmt = util.stateConnect(util.connect(), sql);
			ResultSet rs = stmt.executeQuery();

			ArrayList<ProductForm> saleInfo = new ArrayList<ProductForm>();

			while(rs.next()){
				ProductForm sf = new ProductForm();
				sf.setProductid(rs.getInt("productid"));
				sf.setProductname(rs.getString("productname"));
				sf.setProductimage(rs.getString("productimage"));
				sf.setPrice(rs.getInt("price"));
				saleInfo.add(sf);
			}

			//データベース、ステートメント切断
			util.disStatement(stmt);
			util.disConnect(util.con);

			request.setAttribute("saleInfo",saleInfo);

		}catch(Exception e){

		}

		return (mapping.findForward("success"));
	}
}
