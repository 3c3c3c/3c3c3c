package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.SearchForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SearchCategoryAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentTypeの設定
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		SearchForm searchForm = (SearchForm) form;
		String cate =searchForm.getCate();
		ArrayList<SearchForm> cateResult = new ArrayList<SearchForm>();

		try{
			//データベース接続
			DbUtil util = new DbUtil();

			if(cate.equals("ボーダー柄")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='1';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);

			}else if(cate.equals("柄もの")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='2';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("その他")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='3';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("ハーフパンツ")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='4';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("ショートパンツ")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='5';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("長ズボン")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='6';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("コート")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='7';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("ジャケット")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='8';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("ベスト")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='9';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("キャップ")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='10';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("ハット")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='11';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("ニット帽")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='12';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("スニーカー")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='13';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("サンダル")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='14';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}else if(cate.equals("長ぐつ")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM PRODUCT_TABLE WHERE category ='15';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					SearchForm sf = new SearchForm();
					sf.setProductid(rs.getInt("productid"));
					sf.setProductname(rs.getString("productname"));
					sf.setProductimage(rs.getString("productimage"));
					sf.setPrice(rs.getString("price"));
					cateResult.add(sf);
				}
				DbUtil.disStatement(stmt);
			}
			request.setAttribute("cateResult", cateResult);

			//データベース切断
			DbUtil.disConnect(util.con);

		}catch(Exception ex){
			System.out.println("b");
			System.out.println(ex);
		}
		return (mapping.findForward("success"));
	}
}
