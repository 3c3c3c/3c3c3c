package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.mail.RePassMail;
import jp.co.ccc.user.form.RePassMailForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RePasswordAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		/*
		 * メールアドレスと名前を読み込む
		 * データベースに存在するユーザと判定したら
		 * 読み込んだ後にメールを送信する
		 * 存在しないユーザの場合エラーを返す
		 */

		RePassMailForm repassmail = (RePassMailForm)form;
		String mail = repassmail.getMail();
		String username_family = repassmail.getUsername_family();
		String username_first = repassmail.getUsername_first();
		Boolean flg = true;

		DbUtil util = new DbUtil();

		String sql = "select mail,username_family,username_first from USER_TABLE where mail = '"+mail+"' and username_family = '"+username_family+"' and username_first = '"+username_first+"'";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);
		ResultSet rs = pstmt.executeQuery();

		while(rs.next()){
			flg = false;
		}

		util.disStatement(pstmt);
		util.disConnect(util.con);

		if(flg){
			request.setAttribute("mail", mail);
			return (mapping.findForward("failed"));
		}else{
			RePassMail sendmail = new RePassMail();
			String temppass = sendmail.mail(mail);
			request.setAttribute("temppass", temppass);
			return (mapping.findForward("success"));
		}
	}
}
