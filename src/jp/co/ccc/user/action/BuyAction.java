package jp.co.ccc.user.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.entity.CartEntity;
import jp.co.ccc.user.form.UserInfoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/*
*
*
*/

public class BuyAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		ArrayList<CartEntity> cartent = (ArrayList<CartEntity>)session.getAttribute("cartent");
		if( cartent == null ){
			return (mapping.findForward("failed"));
		}
		else if( cartent.size() == 0 ){
			return (mapping.findForward("failed"));
		}

		if( session.getAttribute("login") == null ){
			session.setAttribute("path", "buy");
			return (mapping.findForward("login"));
		}

		Connection con = null;
		PreparedStatement stmt = null;
		try{
			UserInfoForm user = (UserInfoForm)session.getAttribute("login");
			int id = user.getUserid();
			con = DbUtil.connect();
			String sql = "SELECT * FROM USER_TABLE WHERE userid = ?;";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while( !rs.next() ){
				return (mapping.findForward("login"));
			}

			UserInfoForm add = new UserInfoForm();
			add.setUserid(rs.getInt("userid"));
			add.setUsername_family(rs.getString("username_family"));
			add.setUsername_first(rs.getString("username_first"));
			add.setUsername_family_ruby(rs.getString("username_family_ruby"));
			add.setUsername_first_ruby(rs.getString("username_first_ruby"));
			add.setPostal(rs.getString("postal"));
			add.setAdr_prefectural(rs.getString("adr_prefectural"));
			add.setAdr_city(rs.getString("adr_city"));
			add.setAdr_town(rs.getString("adr_town"));
			add.setAdr_location(rs.getString("adr_location"));
			add.setTel(rs.getString("tel"));
			add.setMail(rs.getString("mail"));
			add.setPass("pass");

			session.setAttribute("add", add);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}

		return (mapping.findForward("success"));
	}
}
