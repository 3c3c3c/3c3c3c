package jp.co.ccc.user.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.entity.CartEntity;
import jp.co.ccc.user.form.ProductForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CartUpdateAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProductForm productForm = (ProductForm)form;

		int id = productForm.getProductid();
		int num = productForm.getNum();

		HttpSession session = request.getSession();

		ArrayList<CartEntity> cartent = (ArrayList<CartEntity>)session.getAttribute("cartent");

		(cartent.get(id)).setQuantity(num);

		session.setAttribute("cartent", cartent);

		return (mapping.findForward("success"));
	}
}
