package jp.co.ccc.user.action;

import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.UserInfoForm;
import jp.co.ccc.user.form.WithDrawForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class WithdrawConfirmAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		WithDrawForm withDrawForm = (WithDrawForm) form;

		//ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		//セッション情報の取得
		HttpSession session = request.getSession();

		UserInfoForm user = new UserInfoForm();
		user = (UserInfoForm)session.getAttribute("login");
		int idnum = user.getUserid();

		//該当ユーザの削除
		DbUtil util = new DbUtil();

		String sql = "DELETE FROM USER_TABLE WHERE userid = " + idnum + ";";

		PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(), sql);
		int rs = stmt.executeUpdate();

		DbUtil.disStatement(stmt);

		session.removeAttribute("login");

	return (mapping.findForward("success"));
	}
}
