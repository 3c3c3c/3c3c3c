package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.RePasswordForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RePasswordSendMailAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		/*
		 * 仮パスワードを認証するクラス
		 * 認証できた場合に次の画面へ遷移する
		 * 認証できない場合はエラーを返す
		 * */

		RePasswordForm repassword = (RePasswordForm)form;
		String mail = repassword.getMail();
		Boolean flg = true;

		DbUtil util = new DbUtil();

 		String sql = "select temppass from TEMPPASS_TABLE where temppass = '"+mail+"'";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);

		ResultSet rs = pstmt.executeQuery();

		while(rs.next()){
			flg = false;
		}

		util.disStatement(pstmt);
		util.disConnect(util.con);

		if(flg){
			return (mapping.findForward("failed"));
		}else{
			return (mapping.findForward("success"));
		}
	}
}
