package jp.co.ccc.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.user.form.DeliveryForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeliveryAction extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		DeliveryForm deliveryForm = (DeliveryForm)form;

		DeliveryForm deli = new DeliveryForm();

		deli.setUserid(deliveryForm.getUserid());
		deli.setUsername_family(deliveryForm.getUsername_family());
		deli.setUsername_first(deliveryForm.getUsername_first());
		deli.setUsername_family_ruby(deliveryForm.getUsername_family_ruby());
		deli.setUsername_first_ruby(deliveryForm.getUsername_first_ruby());
		deli.setAdr_prefectural(deliveryForm.getAdr_prefectural());
		deli.setAdr_city(deliveryForm.getAdr_city());
		deli.setAdr_town(deliveryForm.getAdr_town());
		deli.setAdr_location(deliveryForm.getAdr_location());
		deli.setTel(deliveryForm.getTel());
		deli.setMail(deliveryForm.getMail());
		deli.setDay(deliveryForm.getDay());
		deli.setTime(deliveryForm.getTime());


		HttpSession session = request.getSession();
		session.setAttribute("deli", deli);

		return (mapping.findForward("success"));
	}
}
