package jp.co.ccc.user.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.user.form.UserInfoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MakeUserAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserInfoForm userInfoForm = (UserInfoForm)form;

			//各入力値を受け取る

		if( !userInfoForm.getPass().equals(userInfoForm.getRepass()) ){
			return (mapping.findForward("failed"));
		}

		ArrayList<String> array = new ArrayList<String>();

		array.add(userInfoForm.getUsername_family());
		array.add(userInfoForm.getUsername_first());
		array.add(userInfoForm.getUsername_family_ruby());
		array.add(userInfoForm.getUsername_first_ruby());
		array.add(userInfoForm.getPostal());
		array.add(userInfoForm.getAdr_prefectural());
		array.add(userInfoForm.getAdr_city());
		array.add(userInfoForm.getAdr_town());
		array.add(userInfoForm.getAdr_location());
		array.add(userInfoForm.getTel());
		array.add(userInfoForm.getMail());
		array.add(userInfoForm.getPass());
		array.add(userInfoForm.getRepass());

		request.setAttribute("array", array);

		return (mapping.findForward("success"));
	}
}
