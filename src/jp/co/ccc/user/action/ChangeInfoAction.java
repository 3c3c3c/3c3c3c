package jp.co.ccc.user.action;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.UserInfoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ChangeInfoAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserInfoForm userInfoForm = (UserInfoForm)form;

		//ごめん、処理の流れがわからない
		//↑やっといた

		HttpSession session = request.getSession();
		UserInfoForm user = (UserInfoForm)session.getAttribute("login");
		int userid = user.getUserid();

		Connection con = null;
		PreparedStatement stmt = null;
		String sql = "";
		try{
			con = DbUtil.connect();
			sql = "UPDATE USER_TABLE SET username_family = ?, username_first = ?, "+
										"username_family_ruby = ?, username_first_ruby = ?, "+
										"postal = ?, adr_prefectural = ?, "+
										"adr_city = ?, adr_town = ?, "+
										"tel = ?, mail = ? WHERE userid = ?;";
			stmt = con.prepareStatement(sql);

			stmt.setString(1, userInfoForm.getUsername_family());
			stmt.setString(2, userInfoForm.getUsername_first());
			stmt.setString(3, userInfoForm.getUsername_family_ruby());
			stmt.setString(4, userInfoForm.getUsername_first_ruby());
			stmt.setString(5, userInfoForm.getPostal());
			stmt.setString(6, userInfoForm.getAdr_prefectural());
			stmt.setString(7, userInfoForm.getAdr_city());
			stmt.setString(8, userInfoForm.getAdr_town());
			stmt.setString(9, userInfoForm.getTel());
			stmt.setString(10, userInfoForm.getMail());
			stmt.setInt(11, userid);

			stmt.executeUpdate();
			DbUtil.disStatement(stmt);

			if( !userInfoForm.getAdr_location().equals("") ){
				sql = "UPDATE USER_TABLE SET adr_location = ? WHERE userid = ?;";
				stmt = con.prepareStatement(sql);

				stmt.setString(1, userInfoForm.getAdr_location());
				stmt.setInt(2, userid);

				stmt.executeUpdate();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return (mapping.findForward("failed"));
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}

		return (mapping.findForward("success"));
	}
}
