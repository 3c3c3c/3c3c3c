package jp.co.ccc.user.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.LoginForm;
import jp.co.ccc.user.form.UserInfoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		LoginForm loginForm = (LoginForm)form;

		HttpSession session = request.getSession();

		String mail = loginForm.getMail();
		String pass = loginForm.getPass();
		UserInfoForm user = new UserInfoForm();

		Connection con = null;
		PreparedStatement stmt = null;
		try{
			con = DbUtil.connect();
			String sql = "SELECT * FROM USER_TABLE WHERE mail = ? AND pass = ?;";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, mail);
			stmt.setString(2, pass);
			ResultSet rs = stmt.executeQuery();

			while( !rs.next() ){
				return (mapping.findForward("failed"));
			}
			user.setUserid(rs.getInt("userid"));
			user.setUsername_family(rs.getString("username_family"));
			user.setUsername_first(rs.getString("username_first"));
			session.setAttribute("login", user);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			DbUtil.disStatement(stmt);
			DbUtil.disConnect(con);
		}

		/* ログイン認証後購入画面へ飛ぶための処理 */
		if( request.getParameter("path").equals("buy") ){
			session.setAttribute("path", "login");
			return (mapping.findForward("buy"));
		}

		return (mapping.findForward("success"));
	}
}
