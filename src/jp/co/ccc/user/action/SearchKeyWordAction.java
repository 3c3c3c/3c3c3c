package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.SearchForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SearchKeyWordAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentTypeの設定
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		SearchForm searchForm = (SearchForm)form;
		String keyword = searchForm.getKeyword();
		try{

			//データベース接続
			DbUtil util = new DbUtil();

			//keywordに沿った情報をPRODUCT_TABLEから取得
			String sql ="SELECT * FROM PRODUCT_TABLE WHERE productname like '%" + keyword + "%';";
			PreparedStatement stmt = util.stateConnect(util.connect(), sql);
			ResultSet rs = stmt.executeQuery();

			ArrayList<SearchForm> iteminfo = new ArrayList<SearchForm>();

			while(rs.next()){
				SearchForm sf = new SearchForm();
				sf.setProductid(rs.getInt("productid"));
				sf.setProductname(rs.getString("productname"));
				sf.setProductimage(rs.getString("productimage"));
				sf.setPrice(rs.getString("price"));
				iteminfo.add(sf);
			}

			//データベース、ステートメント切断
			util.disStatement(stmt);
			util.disConnect(util.con);

			request.setAttribute("iteminfo",iteminfo);

		}catch(Exception e){
			System.out.println("error");
		}


		return (mapping.findForward("success"));
		}
}
