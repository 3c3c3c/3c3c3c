package jp.co.ccc.user.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.ProductForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DetailAction extends Action{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentTypeの設定
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		ProductForm productForm = (ProductForm)form;
		int productid = productForm.getProductid();

		try{
			//データベース接続
			DbUtil util = new DbUtil();
			String sql = "select * from PRODUCT_TABLE where productid =?;";

			//ステートメント作成
			PreparedStatement stmt = util.stateConnect(util.connect(),sql);
			stmt.setInt(1,productid);
			ResultSet rs = stmt.executeQuery();

			ArrayList<ProductForm> detail = new ArrayList<ProductForm>();

			while(rs.next()){
				ProductForm pf = new ProductForm();
				pf.setProductid(rs.getInt("productid"));
				pf.setProductname(rs.getString("productname"));
				pf.setCategory(rs.getInt("category"));
				pf.setPrice(rs.getInt("price"));
				pf.setStock(rs.getInt("stock"));
				pf.setProductimage(rs.getString("productimage"));
				pf.setMeasure(rs.getString("measure"));
				pf.setTransaction(rs.getString("transaction"));
				pf.setComment(rs.getString("comment"));

				detail.add(pf);
			}

			request.setAttribute("detail",detail );


			util.disStatement(stmt);
			//データベース切断
			util.disConnect(util.con);


		}catch(Exception e){

		}
		return (mapping.findForward("success"));
	}
}
