package jp.co.ccc.admin.form;

import org.apache.struts.validator.ValidatorForm;

public class AdminForm extends ValidatorForm{

	private int adminid;
	private String pass;
	private String adminname;


	public String getAdminname() {
		return adminname;
	}
	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}
	public int getAdminid() {
		return adminid;
	}
	public void setAdminid(int adminid) {
		this.adminid = adminid;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}


}
