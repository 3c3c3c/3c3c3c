package jp.co.ccc.admin.form;

import org.apache.struts.validator.ValidatorForm;

public class KeywordForm extends ValidatorForm{

	private String keyword;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
}
