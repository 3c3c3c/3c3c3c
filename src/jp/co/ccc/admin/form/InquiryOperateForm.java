package jp.co.ccc.admin.form;

import org.apache.struts.validator.ValidatorForm;

public class InquiryOperateForm extends ValidatorForm{

	private int inquiryid;
	private String inquiry;
	private String choice1;
	private String choice2;
	private String choice3;
	private String choice4;
	private String choice5;


	public int getInquiryid() {
		return inquiryid;
	}
	public void setInquiryid(int inquiryid) {
		this.inquiryid = inquiryid;
	}
	public String getInquiry() {
		return inquiry;
	}
	public void setInquiry(String inquiry) {
		this.inquiry = inquiry;
	}
	public String getChoice1() {
		return choice1;
	}
	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}
	public String getChoice2() {
		return choice2;
	}
	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}
	public String getChoice3() {
		return choice3;
	}
	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}
	public String getChoice4() {
		return choice4;
	}
	public void setChoice4(String choice4) {
		this.choice4 = choice4;
	}
	public String getChoice5() {
		return choice5;
	}
	public void setChoice5(String choice5) {
		this.choice5 = choice5;
	}

}
