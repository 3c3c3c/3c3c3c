package jp.co.ccc.admin.form;

import org.apache.struts.validator.ValidatorForm;

public class UserForm extends ValidatorForm{

	private String keyword;
	private String cate;
	private int userid;
	private int[] delete = new int[30];

	public int[] getDelete() {
		return delete;
	}
	public void setDelete(int[] delete) {
		this.delete = delete;
	}

	private String username_family;
	private String username_first;
	private String username_family_ruby;
	private String username_first_ruby;
	private String postal;
	private String adr_prefectural;
	private String adr_city;
	private String adr_town;
	private String adr_location;
	private String tel;
	private String mail;
	private String pass;
	private String repass;



	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getCate() {
		return cate;
	}
	public void setCate(String cate) {
		this.cate = cate;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername_family() {
		return username_family;
	}
	public void setUsername_family(String username_family) {
		this.username_family = username_family;
	}
	public String getUsername_first() {
		return username_first;
	}
	public void setUsername_first(String username_first) {
		this.username_first = username_first;
	}
	public String getUsername_family_ruby() {
		return username_family_ruby;
	}
	public void setUsername_family_ruby(String username_family_ruby) {
		this.username_family_ruby = username_family_ruby;
	}
	public String getUsername_first_ruby() {
		return username_first_ruby;
	}
	public void setUsername_first_ruby(String username_first_ruby) {
		this.username_first_ruby = username_first_ruby;
	}
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	public String getAdr_prefectural() {
		return adr_prefectural;
	}
	public void setAdr_prefectural(String adr_prefectural) {
		this.adr_prefectural = adr_prefectural;
	}
	public String getAdr_city() {
		return adr_city;
	}
	public void setAdr_city(String adr_city) {
		this.adr_city = adr_city;
	}
	public String getAdr_town() {
		return adr_town;
	}
	public void setAdr_town(String adr_town) {
		this.adr_town = adr_town;
	}
	public String getAdr_location() {
		return adr_location;
	}
	public void setAdr_location(String adr_location) {
		this.adr_location = adr_location;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getRepass() {
		return repass;
	}
	public void setRepass(String repass) {
		this.repass = repass;
	}








}
