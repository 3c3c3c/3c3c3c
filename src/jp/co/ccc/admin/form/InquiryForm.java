package jp.co.ccc.admin.form;

import org.apache.struts.validator.ValidatorForm;

public class InquiryForm extends ValidatorForm{

	private int inquiryid = 101;
	private String inquiry;
	private String choice1;
	private String choice2;
	private String choice3;
	private String choice4;
	private String choice5;
	private int result1;
	private int result2;
	private int result3;
	private int result4;
	private int result5;
	private String num;

	public int getInquiryid() {
		return inquiryid;
	}
	public void setInquiryid(int inquiryid) {
		this.inquiryid = inquiryid;
	}
	public String getInquiry() {
		return inquiry;
	}
	public void setInquiry(String inquiry) {
		this.inquiry = inquiry;
	}
	public String getChoice1() {
		return choice1;
	}
	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}
	public String getChoice2() {
		return choice2;
	}
	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}
	public String getChoice3() {
		return choice3;
	}
	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}
	public String getChoice4() {
		return choice4;
	}
	public void setChoice4(String choice4) {
		this.choice4 = choice4;
	}
	public String getChoice5() {
		return choice5;
	}
	public void setChoice5(String choice5) {
		this.choice5 = choice5;
	}
	public int getResult1() {
		return result1;
	}
	public void setResult1(int result1) {
		this.result1 = result1;
	}
	public int getResult2() {
		return result2;
	}
	public void setResult2(int result2) {
		this.result2 = result2;
	}
	public int getResult3() {
		return result3;
	}
	public void setResult3(int result3) {
		this.result3 = result3;
	}
	public int getResult4() {
		return result4;
	}
	public void setResult4(int result4) {
		this.result4 = result4;
	}
	public int getResult5() {
		return result5;
	}
	public void setResult5(int result5) {
		this.result5 = result5;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
}
