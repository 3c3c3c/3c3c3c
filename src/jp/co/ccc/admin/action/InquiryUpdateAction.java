package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * inquiryupdate.jsp��nquiryUpdateAction��
 * inquiryupdatechoice.jsp��nquiryUpdateChoiceAction.jsp��
 * inquiryupdateInput.jsp��nquiryUpdateInputAction��
 * inquiryupdateConfirm.jsp��nquiryUpdateConfirmAction��
 * inquirychoice.jsp
 */
public class InquiryUpdateAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		// ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		ArrayList<String> inquiry = new ArrayList<String>();
		ArrayList<Integer> num = new ArrayList<Integer>();

		//�f�[�^�x�[�X�ڑ�
		DbUtil util = new DbUtil();

		String sql = "select inquiryid , inquiry from INQUIRY_TABLE ORDER BY inquiryid;";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);

		//INQUIRY_TABLE���玿��𒊏o
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()){

				inquiry.add(rs.getString("inquiry"));
				num.add(rs.getInt("inquiryid"));
				System.out.println(inquiry);
				System.out.println(num);

		}

		//�f�[�^�x�[�X�ؒf
		util.disStatement(pstmt);
		util.disConnect(util.con);

		//setAttribute��inquiry��n��
		request.setAttribute("inquiry",inquiry);
		request.setAttribute("num",num);

		// inquiryupdatechoice.jsp��
		return (mapping.findForward("success"));
	}
}
