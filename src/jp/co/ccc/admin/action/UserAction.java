package jp.co.ccc.admin.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;
import jp.co.ccc.user.form.UserInfoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UserAction extends Action{
	@SuppressWarnings("finally")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		// ContentTypeを設定
				response.setContentType("text/html; charset=UTF-8");
				request.setCharacterEncoding("UTF-8");

				ArrayList<UserInfoForm> usertable = new ArrayList<UserInfoForm>();


				try {
					//JDBC Driverの登録
					Class.forName("org.postgresql.Driver");

					//データベース接続(
					Connection con =DbUtil.connect();

					String sql;					//SQL実行文用変数
					PreparedStatement stmt1;	//Statement接続用変数
					ResultSet rs1;				//SQL実行結果用変数

					//ユーザー全件検索
						sql="SELECT * FROM USER_TABLE;";
						stmt1 = con.prepareStatement(sql);
						rs1 = stmt1.executeQuery();


					//結果を取得し情報を格納
					while(rs1.next()) {
						UserInfoForm usertable1 = new UserInfoForm();
						usertable1.setUserid(rs1.getInt("userid"));
						usertable1.setUsername_family(rs1.getString("username_family"));
						usertable1.setUsername_first(rs1.getString("username_first"));
						usertable1.setUsername_family_ruby(rs1.getString("username_family_ruby"));
						usertable1.setUsername_first_ruby(rs1.getString("username_first_ruby"));
						usertable1.setAdr_prefectural(rs1.getString("adr_prefectural"));
						usertable1.setAdr_city(rs1.getString("adr_city"));
						usertable1.setAdr_town(rs1.getString("adr_town"));
						usertable1.setAdr_location(rs1.getString("adr_location"));
						usertable1.setTel(rs1.getString("tel"));
						usertable1.setMail(rs1.getString("mail"));
						usertable.add(usertable1);
					}

					//データベース切断
					con.close();

				} catch (ClassNotFoundException e) {
					System.out.println("a");
					e.printStackTrace();
				}catch (Exception e) {
					System.out.println("b");
					e.printStackTrace();

				}finally{

					//画面遷移
					request.setAttribute("usertable", usertable);

		return (mapping.findForward("success"));
			}
		}
	}

