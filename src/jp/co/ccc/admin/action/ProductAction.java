package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.ProductForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProductAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProductForm productForm = (ProductForm)form;

		int productid = productForm.getProductid();

		try{
			//ArrayList<ProductForm> list = new ArrayList<ProductForm>();

			//�ｽf�ｽ[�ｽ^�ｽx�ｽ[�ｽX�ｽﾚ托ｿｽ
			//DbUtil util = new DbUtil();
			//�ｽﾎ会ｿｽ�ｽ�ｽ�ｽ�ｽID�ｽﾌ擾ｿｽ�ｽi�ｽ�ｽ�ｽ�ｽ�ｽ謫ｾ
			String sql ="SELECT * FROM PRODUCT_TABLE WHERE productid = ?;";
			PreparedStatement pstmt = DbUtil.stateConnect(DbUtil.connect(), sql);
			pstmt.setInt(1, productid);
			ResultSet rs = pstmt.executeQuery();

			while(rs.next()){
				ProductForm sf = new ProductForm();
				sf.setProductid(rs.getInt("productid"));
				sf.setName(rs.getString("productname"));
				sf.setGenre(rs.getInt("category"));
				sf.setPrice(rs.getInt("price"));
				sf.setStock(rs.getInt("stock"));
				sf.setImage(rs.getString("productimage"));
				sf.setWay(rs.getString("measure"));
				sf.setManagement(rs.getString("transaction"));
				sf.setComment(rs.getString("comment"));

				request.setAttribute("productdata", sf);
				//list.add(sf);
			}

			DbUtil.disStatement(pstmt);
			DbUtil.disConnect(DbUtil.con);

			//request.setAttribute("productdata", list);
		}
		catch(Exception e){
			e.printStackTrace();
			return (mapping.findForward("failed"));
		}

	return (mapping.findForward("success"));
	}
}
