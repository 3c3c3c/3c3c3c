package jp.co.ccc.admin.action;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.ccc.admin.form.AdminForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AdminAction extends Action{
	@SuppressWarnings("finally")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {


		boolean flg = false;//ログイン判定に使用するflgを宣言


		//文字コードを定義
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");


		//入力されたIDとPASSを取得
		AdminForm LoginForm = (AdminForm) form;
		int inputid = LoginForm.getAdminid();
		String inputpass = LoginForm.getPass();


		//セッションを作成
		HttpSession session = request.getSession();

		//セッションに格納するlogintableの宣言
		AdminForm logintable = new AdminForm();

		try{
			//JDBC Driverの登録
			Class.forName("org.postgresql.Driver");

			//データベース接続(
			Connection con =DbUtil.connect();

			//USER_TABLEを全検索
			String sql1="SELECT * FROM ADMIN_TABLE;";
			PreparedStatement stmt1 = con.prepareStatement(sql1);
			ResultSet rs1 = stmt1.executeQuery();

			//ログイン情報の判断に使用する変数を宣言
			boolean hitFlg = false;

			//ADMIN_TABLEに打ち込んだIDとpassがあるか判定
			while(rs1.next()) {
				if(inputid == rs1.getInt("adminid") && inputpass.equals(rs1.getString("adminpass"))){

					int adminid = rs1.getInt("adminid");
					session.setAttribute("adminid",adminid);

					String adminpass = rs1.getString("adminpass");
					session.setAttribute("adminpass",adminpass);

					hitFlg = true;
					break;
				}
			}
			//ADMIN_TABLEに情報があった場合、adminnameの情報も引き出す
			if(hitFlg){
				int id=logintable.getAdminid();
				sql1="SELECT adminname FROM ADMIN_TABLE WHERE adminid=?;";
				stmt1 = con.prepareStatement(sql1);
				stmt1.setInt(1, id);
				rs1 = stmt1.executeQuery();

				while(rs1.next()) {
					System.out.println(rs1.getString("adminname"));
					logintable.setAdminname(rs1.getString("adminname"));
				}
				flg=true;
			}else{
			}

			//データベース切断
			con.close();

		}catch(NumberFormatException e){
		}catch(SQLException e){
		}catch(Exception e){

			//遷移先を決める処理
		}finally{
			if(flg){
				session.setAttribute("logintable",logintable);
				//画面遷移
				return mapping.findForward("success");
			}else{
				request.setAttribute("e", "IDまたはパスワードが間違っています");
				//画面遷移
				return mapping.findForward("failed");
			}

		}
	}
}

