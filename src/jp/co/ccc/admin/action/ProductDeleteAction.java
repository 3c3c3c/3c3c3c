package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.ProductForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProductDeleteAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProductForm productForm = (ProductForm)form;

		int productid = productForm.getProductid();

		//ContentTypeの設定
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		try{
			ArrayList<ProductForm> list = new ArrayList<ProductForm>();

			//データベース接続
			DbUtil util = new DbUtil();
			//対応するIDの商品情報を取得
			String sql ="SELECT * FROM PRODUCT_TABLE WHERE productid = ?;";
			PreparedStatement pstmt = util.stateConnect(util.connect(), sql);
			pstmt.setInt(1,productid);
			ResultSet rs = pstmt.executeQuery();

			while(rs.next()){
				ProductForm sf = new ProductForm();
				sf.setProductid(rs.getInt("productid"));
				sf.setName(rs.getString("productname"));
				sf.setGenre(rs.getInt("category"));
				sf.setPrice(rs.getInt("price"));
				sf.setStock(rs.getInt("stock"));
				sf.setImage(rs.getString("productimage"));
				sf.setWay(rs.getString("measure"));
				sf.setManagement(rs.getString("transaction"));
				sf.setComment(rs.getString("comment"));

				list.add(sf);
			}

			DbUtil.disStatement(pstmt);

			request.setAttribute("productdata", list);


		}catch(Exception e){
			e.printStackTrace();
		}

		return (mapping.findForward("success"));
	}
}
