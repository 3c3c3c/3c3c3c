package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.ProductForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProductDeleteConfirmAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProductForm productForm = (ProductForm)form;

		int productid = productForm.getProductid();

		//ContentTypeの設定
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		try{
			//データベース接続
			DbUtil util = new DbUtil();
			//対応するIDの商品を削除
			String sql ="DELETE FROM PRODUCT_TABLE WHERE productid = ?;";
			PreparedStatement pstmt = util.stateConnect(util.connect(), sql);
			pstmt.setInt(1,productid);
			int rs = pstmt.executeUpdate();

			DbUtil.disStatement(pstmt);

			request.setAttribute("productid", productid);


		}catch(Exception e){
			e.printStackTrace();
		}

		return (mapping.findForward("success"));
	}
}
