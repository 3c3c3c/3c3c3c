package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.InquiryForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * アンケートを削除するクラス
 */
public class InquiryDeleteConfirmAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		InquiryForm inqForm = (InquiryForm)form;
		String num = (String) inqForm.getNum();
		int num1 = Integer.parseInt(num);

		//データベース接続
		DbUtil util = new DbUtil();

		//削除するSQLを代入
		String sql2 = "delete from INQUIRY_TABLE where inquiryid = "+num1+"";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql2);

		//INQUIRY_TABLEから削除する
		int rs2 = pstmt.executeUpdate();
		util.disStatement(pstmt);

		sql2 = "delete from INQUIRYRESULT_TABLE where inquiryid = "+num1+"";
		pstmt = util.stateConnect(util.connect(),sql2);

		//INQUIRY_TABLEから削除する
		rs2 = pstmt.executeUpdate();

		request.setAttribute("id", num1);

		//データベース切断
		util.disStatement(pstmt);
		util.disConnect(util.con);
		return (mapping.findForward("success"));
	}
}