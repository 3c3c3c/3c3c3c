package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.InquiryForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/*
 * inquirydelete.jsp
 */
public class InquiryDeleteChoiceAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		//
		InquiryForm inqForm = (InquiryForm)form;
		int num = inqForm.getInquiryid();


		try{
			ArrayList<String> inquiry = new ArrayList<String>();

				//
				DbUtil util = new DbUtil();

				String sql = "select inquiry,choice1,choice2,choice3,choice4,choice5 from INQUIRY_TABLE where inquiryid = "+num+"";
				PreparedStatement pstmt = util.stateConnect(util.connect(),sql);
				ResultSet rs = pstmt.executeQuery();

				while(rs.next()){
					inquiry.add(rs.getString("inquiry"));
					inquiry.add(rs.getString("choice1"));
					inquiry.add(rs.getString("choice2"));
					inquiry.add(rs.getString("choice3"));
					inquiry.add(rs.getString("choice4"));
					inquiry.add(rs.getString("choice5"));
				}

				String str_num = Integer.toString(num);
				util.disStatement(pstmt);
				util.disConnect(util.con);

			request.setAttribute("inquiry",inquiry);
			request.setAttribute("num", str_num);

		}catch(Exception ex){
			System.out.println(ex);
		}
		return (mapping.findForward("success"));
	}
}