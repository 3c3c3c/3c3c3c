package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/*
 * INQUIRY_TABLEから情報を抽出する
 * INQUIRY_TABLEから質問内容の身を検索
 * 検索結果をArrayList<String>へ格納
 * inquirydelete.jspへ遷移
 */
public class InquiryDeleteAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		ArrayList<String> inquiry = new ArrayList<String>();
		ArrayList<Integer> num = new ArrayList<Integer>();

		//データベース接続
		DbUtil util = new DbUtil();

		String sql = "select inquiryid , inquiry from INQUIRY_TABLE ORDER BY inquiryid;";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);

		//INQUIRY_TABLEから質問を抽出
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()){
				inquiry.add(rs.getString("inquiry"));
				num.add(rs.getInt("inquiryid"));
		}

		//データベース切断
		util.disStatement(pstmt);
		util.disConnect(util.con);

		//setAttributeでinquiryを渡す
		request.setAttribute("inquiry",inquiry);
		request.setAttribute("num",num);

		//inquiryupdatechoice.jspへ
		return (mapping.findForward("success"));
	}

}
