package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.ProductForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProductUpdateInputAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProductForm productForm = (ProductForm)form;

		int productid = productForm.getProductid();
		String name = productForm.getName();
		int genre = productForm.getGenre();
		int price = productForm.getPrice();
		int stock = productForm.getStock();
		String image = productForm.getImage();
		String way = productForm.getWay();
		String management = productForm.getManagement();
		String comment = productForm.getComment();

		try{
			//データベース接続
			DbUtil util = new DbUtil();
			//対応するIDの商品情報を取得

			String sql ="UPDATE PRODUCT_TABLE SET "
					+ "productname = ? ,"
					+ "category = '" + genre + "', "
					+ "price = ? ,"
					+ "stock = ? ,"
					+ "measure = ? ,"
					+ "comment = ? ,"
					+ "transaction = ? "
					+ " WHERE productid = ?;";
			PreparedStatement pstmt = util.stateConnect(util.connect(), sql);
			pstmt.setString(1, name);
			pstmt.setInt(2, price);
			pstmt.setInt(3, stock);
			pstmt.setString(4, way);
			pstmt.setString(5, comment);
			pstmt.setString(6, management);
			pstmt.setInt(7, productid);

			int rs = pstmt.executeUpdate();

			DbUtil.disStatement(pstmt);

			request.setAttribute("productid", productid);

		}catch(Exception e){
			e.printStackTrace();
		}

		return (mapping.findForward("success"));
	}
}
