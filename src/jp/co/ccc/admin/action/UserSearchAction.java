package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.UserForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UserSearchAction extends Action{
	@SuppressWarnings("finally")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		// ContentTypeを設定
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//入力されたキーワードとカテゴリを取得
		UserForm UserForm = (UserForm) form;
		String cate =UserForm.getCate();
		String keyword = UserForm.getKeyword();
		ArrayList<UserForm> usertable = new ArrayList<UserForm>();

		try {

			//データベース接続
			DbUtil util = new DbUtil();

			//入力が無い場合
			if(keyword.equals("")){
				//画面遷移
				return (mapping.findForward("failed"));

			}else if(!(keyword.equals(""))){
				//keywordに沿った情報をUSER_TABLEから取得
				String sql ="SELECT * FROM USER_TABLE WHERE username_family like '%" + keyword +
				"%' or username_first like '%" + keyword +
				"% or userid like %" + keyword +
				"%' or username_family_ruby like '%" + keyword +
				"%' or username_first_ruby  like '%" + keyword +
				"%' or adr_prefectural  like '%" + keyword +
				"%' or adr_city  like '%" + keyword +
				"%' or adr_town  like '%" + keyword +
				"%' or adr_location  like '%" + keyword +
				"%' or tel  like '%" + keyword +
				"%' or mail like  '%" + keyword +
				"%';";
				PreparedStatement stmt = util.stateConnect(util.connect(), sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					UserForm sf = new UserForm();
					sf.setUserid(rs.getInt("userid"));
					sf.setUsername_family(rs.getString("username_family"));
					sf.setUsername_first(rs.getString("username_first"));
					sf.setUsername_family_ruby(rs.getString("username_family_ruby"));
					sf.setUsername_first_ruby(rs.getString("username_first_ruby"));
					sf.setAdr_prefectural(rs.getString("adr_prefectural"));
					sf.setAdr_city(rs.getString("adr_city"));
					sf.setAdr_town(rs.getString("adr_town"));
					sf.setAdr_location(rs.getString("adr_location"));
					sf.setTel(rs.getString("tel"));
					sf.setMail(rs.getString("mail"));
					usertable.add(sf);
				}


			}else if(cate.equals("ID")){
				//カテゴリーに沿った商品をPRODUCT_TABLEから取得
				String sql ="SELECT * FROM USER_TABLE WHERE userid ='1';";

				//ステートメント作成
				PreparedStatement stmt = DbUtil.stateConnect(DbUtil.connect(),sql);
				ResultSet rs = stmt.executeQuery();

				while(rs.next()){
					UserForm sf = new UserForm();
					sf.setUserid(rs.getInt("userid"));
					sf.setUsername_family(rs.getString("username_family"));
					sf.setUsername_first(rs.getString("username_first"));
					sf.setUsername_family_ruby(rs.getString("username_family_ruby"));
					sf.setUsername_first_ruby(rs.getString("username_first_ruby"));
					sf.setAdr_prefectural(rs.getString("adr_prefectural"));
					sf.setAdr_city(rs.getString("adr_city"));
					sf.setAdr_town(rs.getString("adr_town"));
					sf.setAdr_location(rs.getString("adr_location"));
					sf.setTel(rs.getString("tel"));
					sf.setMail(rs.getString("mail"));
					usertable.add(sf);
					System.out.println(rs.getInt("userid"));
				}
				DbUtil.disStatement(stmt);

			}

					//データベース切断
					DbUtil.disConnect(util.con);

				} catch (ClassNotFoundException e) {
					System.out.println("a");
					e.printStackTrace();
				}catch (Exception e) {
					System.out.println("b");
					e.printStackTrace();

				}finally{
					request.setAttribute("usertable", usertable);

					//画面遷移
					return (mapping.findForward("success"));
				}
	}
}

