package jp.co.ccc.admin.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.InquiryForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InquiryUpdateInputAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		InquiryForm inqForm = (InquiryForm) form;
		String inq = (String) inqForm.getInquiry();
		String choice1 = (String) inqForm.getChoice1();
		String choice2 = (String) inqForm.getChoice2();
		String choice3 = (String) inqForm.getChoice3();
		String choice4 = (String) inqForm.getChoice4();
		String choice5 = (String) inqForm.getChoice5();
		String num = (String) inqForm.getNum();

		System.out.println(inq);
		System.out.println(choice1);
		System.out.println(choice2);
		System.out.println(choice3);
		System.out.println(choice4);
		System.out.println(choice5);
		System.out.println(num);

		ArrayList<String> inquiry = new ArrayList<String>();

		inquiry.add(inq);
		inquiry.add(choice1);
		inquiry.add(choice2);
		inquiry.add(choice3);
		inquiry.add(choice4);
		inquiry.add(choice5);

		// setAttribute��inquiry��n��
		request.setAttribute("inquiry", inquiry);
		request.setAttribute("num", num);

		// inquiryupdateconfirm.jsp��
		return (mapping.findForward("success"));
	}
}
