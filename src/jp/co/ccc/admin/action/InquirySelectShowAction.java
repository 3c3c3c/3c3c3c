package jp.co.ccc.admin.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.InquiryForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InquirySelectShowAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		//何番の結果を表示するかの入力値を格納する
		InquiryForm inqForm = (InquiryForm)form;
		/*int num1 = 0;
		int inqid = 101;
		int str_num = Integer.parseInt(inqForm.getInquiry());

		for(;str_num != num1;){
			inqid++;
			num1++;
		}*/
		int id = inqForm.getInquiryid();

		try{
			Connection con = DbUtil.connect();
			//問題内容と結果割合を格納するArrayListの宣言
			ArrayList<Integer> result = new ArrayList<Integer>();
			ArrayList<String> inquiry = new ArrayList<String>();

				//データベース接続して入力された結果のすべてを呼び出す
				DbUtil util = new DbUtil();

				String sql = "select inquiry,choice1,choice2,choice3,choice4,choice5 from INQUIRY_TABLE where inquiryid = ?;";
				PreparedStatement pstmt = con.prepareStatement(sql);
				pstmt.setInt(1, id);
				ResultSet rs = pstmt.executeQuery();

				while(rs.next()){
					inquiry.add(rs.getString("inquiry"));
					inquiry.add(rs.getString("choice1"));
					inquiry.add(rs.getString("choice2"));
					inquiry.add(rs.getString("choice3"));
					inquiry.add(rs.getString("choice4"));
					inquiry.add(rs.getString("choice5"));
				}

				String sql2 = "select result1,result2,result3,result4,result5 from INQUIRYRESULT_TABLE where inquiryid = ?;";
				PreparedStatement pstmt2 = con.prepareStatement(sql2);
				pstmt2.setInt(1, id);
				ResultSet rs2 = pstmt2.executeQuery();

				while(rs2.next()){
					result.add(rs2.getInt("result1"));
					result.add(rs2.getInt("result2"));
					result.add(rs2.getInt("result3"));
					result.add(rs2.getInt("result4"));
					result.add(rs2.getInt("result5"));
				}

				//データベース切断
				DbUtil.disStatement(pstmt);
				DbUtil.disStatement(pstmt2);
				DbUtil.disConnect(con);

			request.setAttribute("inquiry",inquiry);
			request.setAttribute("result",result);

		}catch(Exception ex){
			System.out.println(ex);
		}
		return (mapping.findForward("success"));
	}
}
