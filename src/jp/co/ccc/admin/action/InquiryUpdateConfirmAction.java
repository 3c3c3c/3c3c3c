package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.InquiryForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InquiryUpdateConfirmAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		// ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		InquiryForm inqForm = (InquiryForm) form;
		String inq = (String) inqForm.getInquiry();
		String choice1 = (String) inqForm.getChoice1();
		String choice2 = (String) inqForm.getChoice2();
		String choice3 = (String) inqForm.getChoice3();
		String choice4 = (String) inqForm.getChoice4();
		String choice5 = (String) inqForm.getChoice5();
		String num = (String) inqForm.getNum();
		System.out.println(num);

		// データベースへ接続
		DbUtil util = new DbUtil();

		// 呼び込んだ
		String sql = "update INQUIRY_TABLE set inquiry = '" + inq
				+ "' , choice1 = '" + choice1 + "', choice2 = '" + choice2
				+ "', choice3 = '" + choice3 + "', choice4 = '" + choice4
				+ "', choice5 = '" + choice5 + "' where inquiryid = " + num
				+ ";";
		PreparedStatement pstmt = util.stateConnect(util.connect(), sql);

		// INQUIRY_TABLEへ変更内容を格納
		int rs = pstmt.executeUpdate();

		// データベース切断
		util.disStatement(pstmt);
		util.disConnect(util.con);

		request.setAttribute("id", num);

		// inquirycoice.jspへ
		return (mapping.findForward("success"));
	}
}
