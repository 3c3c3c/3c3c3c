package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.InquiryForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InquiryInsertConfirmAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ContentType
		response.setContentType("text/html; charset=Shift_JIS");
		request.setCharacterEncoding("UTF-8");

		InquiryForm inqForm = (InquiryForm)form;
		ArrayList<String> inquiryList = new ArrayList<String>();
		inquiryList.add((String)inqForm.getInquiry());
		inquiryList.add((String)inqForm.getChoice1());
		inquiryList.add((String)inqForm.getChoice2());
		inquiryList.add((String)inqForm.getChoice3());
		inquiryList.add((String)inqForm.getChoice4());
		inquiryList.add((String)inqForm.getChoice5());

		ArrayList<String> inquiry = new ArrayList<String>();
		int loopnum = 0;
		Iterator<String> it = inquiryList.iterator();

		DbUtil util = new DbUtil();

		while(it.hasNext()){
	    	inquiry.add((String)it.next());
	    	loopnum++;
	    }

		if(inquiry.get(0) == null || inquiry.get(0).equals("") || inquiry.get(1) == null || inquiry.get(1).equals("")){
			return (mapping.findForward("failed"));
		}else {

		String sql = "insert into INQUIRY_TABLE(inquiry, choice1, choice2, choice3, choice4, choice5) values('"+inquiry.get(0)+"','"+inquiry.get(1)+"','"+inquiry.get(2)+"','"+inquiry.get(3)+"','"+inquiry.get(4)+"','"+inquiry.get(5)+"');";
		PreparedStatement pstmt = util.stateConnect(util.connect(),sql);

		int rs = pstmt.executeUpdate();
		util.disStatement(pstmt);

		sql = "SELECT inquiryid FROM INQUIRY_TABLE WHERE inquiry = '"+ inquiry.get(0) +"';";
		pstmt = util.stateConnect(util.connect(),sql);
		ResultSet r = pstmt.executeQuery();
		int id = 0;
		while( r.next() ){
			id = r.getInt("inquiryid");
		}
		util.disStatement(pstmt);

		sql = "insert into INQUIRYRESULT_TABLE values("+id+",0, 0, 0, 0, 0, 0);";
		pstmt = util.stateConnect(util.connect(),sql);
		rs = pstmt.executeUpdate();

		util.disStatement(pstmt);
		util.disConnect(util.con);

		return (mapping.findForward("success"));
		}
	}
}
