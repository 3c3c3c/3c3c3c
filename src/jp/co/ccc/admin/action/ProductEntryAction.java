package jp.co.ccc.admin.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.ProductEntryForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class ProductEntryAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProductEntryForm productEntryForm = (ProductEntryForm) form;

		String name = productEntryForm.getName();
		int genre = productEntryForm.getGenre();
		int price = productEntryForm.getPrice();
		int stock = productEntryForm.getStock();
		String way = productEntryForm.getWay();
		String management = productEntryForm.getManagement();
		String comment = productEntryForm.getComment();

		int Ogenre = new Integer(genre);

		request.setAttribute("name", name);
		request.setAttribute("genre", Ogenre);
		request.setAttribute("price", price);
		request.setAttribute("stock", stock);
		request.setAttribute("way", way);
		request.setAttribute("management", management);
		request.setAttribute("comment", comment);

		FormFile image = productEntryForm.getImage();

		if( image.getFileName().equals("") ){
			return (mapping.findForward("failed"));
		}

		ServletContext sc = getServlet().getServletContext();

		InputStream is = image.getInputStream();

		BufferedInputStream inBuffer = new BufferedInputStream(is);

		FileOutputStream fos = new FileOutputStream
				(sc.getRealPath("/photo")+ "/"
						+ image.getFileName());

		BufferedOutputStream outBuffer = new BufferedOutputStream(fos);

		int contents = 0;

		while ((contents = inBuffer.read()) != -1) {
			outBuffer.write(contents);
		}

		outBuffer.flush();
		inBuffer.close();
		outBuffer.close();

		request.setAttribute("image", image.getFileName());

		image.destroy();

		return (mapping.findForward("success"));
	}
}
