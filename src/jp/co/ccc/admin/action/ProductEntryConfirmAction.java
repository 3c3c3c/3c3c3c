package jp.co.ccc.admin.action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.ProductEntryForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProductEntryConfirmAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		String newname = (String)request.getAttribute("name");

		ProductEntryForm productEntryForm = (ProductEntryForm) form;

		String name = productEntryForm.getName();
		int genre = productEntryForm.getGenre();
		int price = productEntryForm.getPrice();
		int stock = productEntryForm.getStock();
		String imagename = productEntryForm.getImagename();
		String way = productEntryForm.getWay();
		String management = productEntryForm.getManagement();
		String comment = productEntryForm.getComment();

		int size = 80;

		try{
			//�ｽf�ｽ[�ｽ^�ｽx�ｽ[�ｽX�ｽﾚ托ｿｽ
			DbUtil util = new DbUtil();

			//�ｽV�ｽKID�ｽ�ｽ�ｽl�ｽ�ｽ
			ArrayList<ProductEntryForm> list = new ArrayList<ProductEntryForm>();
			String selsql = "SELECT * FROM PRODUCT_TABLE;";
			PreparedStatement selpstmt = util.stateConnect(util.connect(), selsql);
			ResultSet exist = selpstmt.executeQuery();
			while(exist.next()){
				ProductEntryForm expef = new ProductEntryForm();
				expef.setProductid(exist.getInt("productid"));
				list.add(expef);
			}

			//�ｽ�ｽ�ｽ�ｽ�ｽ�ｽID�ｽ�ｽ�ｽi�ｽ[�ｽ�ｽ�ｽ�ｽR�ｽ�ｽ�ｽN�ｽV�ｽ�ｽ�ｽ�ｽ�ｽ�ｽp�ｽ�ｽ
			ArrayList<Integer> check = new ArrayList<Integer>();
			for(int i = 0; i < list.size(); i++){
				check.add(list.get(i).getProductid());
			}
			//ID�ｽﾌ趣ｿｽ�ｽ�ｽ�ｽﾌ費ｿｽ
			int idnum = 0;
			for(idnum = 1001; ;idnum++){
				int count = 0;
				for(int j = 0; j< check.size(); j++){
					if(check.get(j) == idnum){
						count ++;
					}
				}
				if(count == 0){
					break;
				}
			}

			//�ｽﾎ会ｿｽ�ｽ�ｽ�ｽ�ｽID�ｽﾌ擾ｿｽ�ｽi�ｽ�ｽ�ｽ�ｽo�ｽ^
			String sql ="INSERT INTO PRODUCT_TABLE "
					+ "(productid,"		//1
					+ " productname,"	//2
					+ " category,"		//3
					+ " subcategory,"	//4
					+ " price,"			//5
					+ " stock,"			//6
					+ " comment,"		//7
					+ " measure,"		//8
					+ " transaction,"	//9
					+ " productimage) "	//10
					+ "VALUES"
					+ ""
					+ "( ? ,"	//1,id
					+ " ? ,"	//2,name
					+ " ? ,"	//3,genre
					+ " ? ,"	//4,size
					+ " ? ,"	//5,price
					+ " ? ,"	//6,stock
					+ " ? ,"	//7,comment
					+ " ? ,"	//8,way
					+ " ? ,"	//9,management
					+ " ? "		//10,image
					+ ");";
			PreparedStatement pstmt = util.stateConnect(util.connect(), sql);
			pstmt.setInt(1, idnum);
			pstmt.setString(2, name);
			pstmt.setInt(3, genre);
			pstmt.setInt(4, size);
			pstmt.setInt(5, price);
			pstmt.setInt(6, stock);
			pstmt.setString(7, way);
			pstmt.setString(8, management);
			pstmt.setString(9, comment);
			pstmt.setString(10,imagename);

			int rs = pstmt.executeUpdate();

			DbUtil.disStatement(pstmt);

			request.setAttribute("product", idnum);

		}catch(Exception e){
			e.printStackTrace();
		}




		return (mapping.findForward("success"));
	}
}
