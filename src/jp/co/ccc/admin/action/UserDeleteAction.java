package jp.co.ccc.admin.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.UserForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UserDeleteAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		// ContentTypeを設定
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		ArrayList<UserForm> usertable = new ArrayList<UserForm>();

		//値を取得
		UserForm UserForm = (UserForm) form;
		int[] delete = UserForm.getDelete();

		try {
			//データベース接続(
			Connection con =DbUtil.connect();

			String sql;				//SQL実行文用変数
			PreparedStatement stmt;//Statement接続用変数
			ResultSet rs;			//SQL実行結果用変数

			//入力されたidがusertableにあるかを検索

			for(int i=0 ; i<delete.length ; i++){
				sql="SELECT * FROM USER_TABLE WHERE userid = ?";
				stmt = con.prepareStatement(sql);
				stmt.setInt(1, delete[i]);
				rs = stmt.executeQuery();

				while(rs.next()){
					UserForm user = new UserForm();
					user.setUserid(rs.getInt("userid"));
					user.setUsername_family(rs.getString("username_family"));
					user.setUsername_first(rs.getString("username_first"));
					user.setUsername_family_ruby(rs.getString("username_family_ruby"));
					user.setUsername_first_ruby(rs.getString("username_first_ruby"));
					user.setAdr_prefectural(rs.getString("adr_prefectural"));
					user.setAdr_city(rs.getString("adr_city"));
					user.setAdr_town(rs.getString("adr_town"));
					user.setAdr_location(rs.getString("adr_location"));
					user.setTel(rs.getString("tel"));
					user.setMail(rs.getString("mail"));
					usertable.add(user);
				}
				DbUtil.disStatement(stmt);
			}
			request.setAttribute("usertable",usertable);

			//データベース切断
			con.close();

		}catch (Exception eq) {
			eq.printStackTrace();
			return (mapping.findForward("failed"));

		}
		//return null;
		return (mapping.findForward("success"));
	}
}