package jp.co.ccc.admin.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.ccc.admin.form.UserForm;
import jp.co.ccc.dao.DbUtil;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UserDeleteConfirmAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserForm UserForm = (UserForm) form;

		int[] delete = UserForm.getDelete();
		ArrayList<Integer> delList = new ArrayList<Integer>();

		try {
			//データベース接続(
			Connection con =DbUtil.connect();

			String sql;				//SQL実行文用変数
			PreparedStatement stmt;//Statement接続用変数

			//入力されたidがusertableにあるかを検索

			for(int i=0 ; i<delete.length ; i++){
				sql="DELETE FROM USER_TABLE WHERE userid = ?";
				stmt = con.prepareStatement(sql);
				stmt.setInt(1, delete[i]);
				stmt.executeUpdate();

				delList.add(delete[i]);

				DbUtil.disStatement(stmt);
			}
			request.setAttribute("delList",delList);
			//データベース切断
			con.close();

		}catch (Exception eq) {
			eq.printStackTrace();
			return (mapping.findForward("failed"));

		}

		return (mapping.findForward("success"));
	}
}
